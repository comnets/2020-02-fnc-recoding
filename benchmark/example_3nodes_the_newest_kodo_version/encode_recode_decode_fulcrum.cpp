// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF EVALUATION LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <vector>

#include <storage/storage.hpp>

#include <kodo_fulcrum/coders.hpp>
#include <kodo_rlnc/coders.hpp>

#include <boost/random/bernoulli_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>

/// @example encode_decode_fulcrum.cpp
///
/// Simple example showing how to encode and decode a block of data using
/// the Fulcrum codec.
/// For a detailed description of the Fulcrum codec see the following paper
/// on arxiv: http://arxiv.org/abs/1404.6620 by Lucani et. al.

uint32_t symbols = 16;
//erasure probability in the links
double e1 = 0.5, e2 = 0.5;

int main(int argc, char *argv[])
{
    if(argc > 1)
        symbols = boost::lexical_cast<uint32_t>(argv[1]);
    if(argc > 2)
        e1 = boost::lexical_cast<double>(argv[2]);
    if(argc > 3)
        e1 = boost::lexical_cast<double>(argv[3]);
    // Seed the random number generator to get different random data
    srand(static_cast<uint32_t>(time(0)));

    // Specify the coding parameters
    fifi::finite_field field = fifi::finite_field::binary8;
   
    uint32_t symbol_size = 160;

    boost::random::mt19937 random_generator;
    boost::random::bernoulli_distribution<> m_distribution1, m_distribution2;
    random_generator.seed((uint32_t) time(0));
    m_distribution1 = boost::random::bernoulli_distribution<>(e1);
    m_distribution2 = boost::random::bernoulli_distribution<>(e2);

    // The number of expansion symbols is a unique parameter for fulcrum
    // uint32_t expansion = 1;
    
    uint32_t runs = 50000;
    
    for(uint32_t expansion = 1; expansion <= 7; expansion++)
    {
        uint32_t ldp = 0, trans = 0;

        

        for(uint32_t i = 0; i < runs; i++)
        {
           
             // Build an encoder and a decoder
            kodo_fulcrum::encoder encoder(field, symbols, symbol_size, expansion);
            kodo_fulcrum::decoder decoder(field, symbols, symbol_size, expansion);

            //recoder
            kodo_rlnc::decoder recoder(fifi::finite_field::binary, symbols+expansion, symbol_size);

            // Allocate some storage for a "payload" the payload is what we would
            // eventually send over a network
            std::vector<uint8_t> payload(encoder.max_payload_size());

            // Allocate some data to encode. In this case we make a buffer
            // with the same size as the encoder's block size (the max.
            // amount a single encoder can encode)
            std::vector<uint8_t> data_in(encoder.block_size());

            // Just for fun - fill the data with random data
            std::generate(data_in.begin(), data_in.end(), rand);

            // Assign the data buffer to the encoder so that we may start
            // to produce encoded symbols
            encoder.set_symbols_storage(data_in.data());

            // Define a data buffer where the symbols should be decoded
            std::vector<uint8_t> data_out(decoder.block_size());

            std::vector<uint8_t> data_recoder(recoder.block_size());
            recoder.set_symbols_storage(data_recoder.data());

            decoder.set_symbols_storage(data_out.data());

            uint32_t pre_rank = 0;

            // Generate packets until the decoder is complete
            while (!decoder.is_complete())
            {
                // Encode a packet into the payload buffer
                encoder.produce_payload(payload.data());

                trans += 1;

                if(!m_distribution1(random_generator))
                {
                    recoder.consume_payload(payload.data());
                }

                recoder.produce_payload(payload.data());

                if(!m_distribution2(random_generator)){

                    // Pass that packet to the decoder
                    decoder.consume_payload(payload.data());
                    if(decoder.rank() == pre_rank)
                    {
                        ldp += 1;
                    }
                    pre_rank = decoder.rank();
                }
                
            }
        }

        std::cout<<"Expansion Packets:" << expansion<< std::endl;
        std::cout<<"LDP: " << ldp/(runs*1.0) << std::endl;
        std::cout<<"TX : " << trans/(runs*1.0) << std::endl;
    }

    // // Check if we properly decoded the data
    // if (std::equal(data_out.begin(), data_out.end(), data_in.begin()))
    // {
    //     std::cout << "Data decoded correctly" << std::endl;
    // }
    // else
    // {
    //     std::cout << "Unexpected failure to decode, "
    //               << "please file a bug report :)" << std::endl;
    // }
}
