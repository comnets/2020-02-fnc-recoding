// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <gauge/gauge.hpp>

#include <kodo_fulcrum/fulcrum_codes.hpp>
#include <kodo_rlnc/full_vector_codes.hpp>

//#include "fulcrum_transmissions_benchmark_multihop.hpp"
//#include "dsep_transmissions_benchmark_multihop.hpp"

#include "rlnc_throughput_benchmark_multihop.hpp"
//#include "sparse_rlnc_transmissions_benchmark_multihop.hpp"


template<class SuperCoder>
class fulcrum_combined_decoder: public SuperCoder {
public:
	using stage_one_decoder_type = typename SuperCoder::stage_one_decoder_type;
	using stage_two_decoder_type = typename SuperCoder::stage_two_decoder_type;

	/// @return The stage one decoder
	uint32_t stage_one_decoder_rank() {
		return SuperCoder::stage_one_decoder().rank();
	}

	/// @return The stage two decoder
	uint32_t stage_two_decoder_rank() {
		return SuperCoder::stage_two_decoder().rank();
	}

	uint32_t rank()
	{
		return stage_one_decoder_rank() + stage_two_decoder_rank();
	}


	using factory = kodo_core::pool_factory<fulcrum_combined_decoder>;
};

template<class SuperCoder>
class fulcrum_inner_decoder: public SuperCoder {
public:

	uint32_t rank()
	{
		const auto& nested = SuperCoder::nested();
		assert(nested);
		return nested->rank();
	}

	using factory = kodo_core::pool_factory<fulcrum_inner_decoder>;
};


/// Using this macro we may specify options. For specifying options
/// we use the boost program options library. So you may additional
/// details on how to do it in the manual for that library.
BENCHMARK_OPTION(throughput_options)
{
    gauge::po::options_description options;

    std::vector<uint32_t> symbols;
//    symbols.push_back(16);
    symbols.push_back(32);

    auto default_symbols =
        gauge::po::value<std::vector<uint32_t> >()->default_value(
            symbols, "")->multitoken();

    std::vector<uint32_t> symbol_size;
    symbol_size.push_back(1500);

    auto default_symbol_size =
        gauge::po::value<std::vector<uint32_t> >()->default_value(
            symbol_size, "")->multitoken();

    std::vector<uint32_t> expansion;
    expansion.push_back(2);

    auto default_expansion =
        gauge::po::value<std::vector<uint32_t> >()->default_value(
            expansion, "")->multitoken();

    std::vector<bool> recoding;
	recoding.push_back(true);
	auto default_recoding =
		gauge::po::value<std::vector<bool> >()->default_value(
			recoding, "")->multitoken();
	options.add_options()
		  ("recoding", default_recoding, "Set the recoding");

    std::vector<uint32_t> redundant;
	redundant.push_back(5);
	auto default_redundant =
		gauge::po::value<std::vector<uint32_t> >()->default_value(
			redundant, "")->multitoken();
	options.add_options()
		  ("redundant", default_redundant, "Set the redundant");


	std::vector<double> density;
	density.push_back(0.5);
	auto default_density =
	gauge::po::value<std::vector<double> >()->default_value(
				density, "")->multitoken();
	options.add_options()
			  ("density", default_density, "Set density");



    std::vector<std::string> types;
    types.push_back("decoder");
    auto default_types =
            gauge::po::value<std::vector<std::string> >()->default_value(
                types, "")->multitoken();

    std::vector<double> erasures1;
    erasures1.push_back(0);

    auto default_erasures1 =
          gauge::po::value<std::vector<double> >()->default_value(
              erasures1, "")->multitoken();

 std::vector<double> erasures2;
    erasures2.push_back(0);

    auto default_erasures2 =
          gauge::po::value<std::vector<double> >()->default_value(
              erasures2, "")->multitoken();

    std::vector<uint32_t> relays;
    relays.push_back(2);


      auto default_relays =
          gauge::po::value<std::vector<uint32_t> >()->default_value(
              relays, "")->multitoken();

      options.add_options()
              ("num_relays", default_relays, "Set the number of relays");

      std::vector<uint32_t> recodingsymbols;
      recodingsymbols.push_back(3);
        auto default_recsym =
            gauge::po::value<std::vector<uint32_t> >()->default_value(
                recodingsymbols, "")->multitoken();

        options.add_options()
                ("recoding_symbols", default_recsym, "Set the recoding symbols");


    options.add_options()
    ("symbols", default_symbols, "Set the number of symbols");

    options.add_options()
    ("symbol_size", default_symbol_size, "Set the symbol size in bytes");

    options.add_options()
    ("expansions", default_expansion,
     "Set the expansion of the fulcrum codes");

    options.add_options()
    ("type", default_types, "Set type [encoder|decoder]");

    options.add_options()
        ("erasure1", default_erasures1, "Set erasures first link");

    options.add_options()
        ("erasure2", default_erasures2, "Set erasures second link");

    gauge::runner::instance().register_options(options);
}

/////////////////////FullRLNC/////////////////////

using setup_rlnc2 =
    rlnc_throughput_benchmark_multihop<
    kodo_rlnc::full_vector_encoder<fifi::binary>,
    kodo_rlnc::full_vector_recoder<fifi::binary>,
	kodo_rlnc::full_vector_decoder<fifi::binary>>;

BENCHMARK_F_INLINE(setup_rlnc2, RLNC2, Binary, 30)
{
    run_body();
}

using setup_rlnc8 =
    rlnc_throughput_benchmark_multihop<
    kodo_rlnc::full_vector_encoder<fifi::binary8>,
    kodo_rlnc::full_vector_recoder<fifi::binary8>,
	kodo_rlnc::full_vector_decoder<fifi::binary8>>;

BENCHMARK_F_INLINE(setup_rlnc8, RLNC8, Binary8, 30)
{
    run_body();
}

/////////////////////Sparse-FullRLNC/////////////////////

//using setup_sparse_rlnc2 =
//    sparse_rlnc_transmissions_benchmark_multihop<
//    kodo_rlnc::sparse_full_vector_encoder<fifi::binary>,
//    kodo_rlnc::full_vector_recoder<fifi::binary>,
//	kodo_rlnc::full_vector_decoder<fifi::binary>>;
//
//BENCHMARK_F_INLINE(setup_sparse_rlnc2, RLNC2, Binary, 30)
//{
//    run();
//}
//
//using setup_sparse_rlnc8 =
//    sparse_rlnc_transmissions_benchmark_multihop<
//    kodo_rlnc::sparse_full_vector_encoder<fifi::binary8>,
//    kodo_rlnc::full_vector_recoder<fifi::binary8>,
//	kodo_rlnc::full_vector_decoder<fifi::binary8>>;
//
//BENCHMARK_F_INLINE(setup_sparse_rlnc8, RLNC8, Binary8, 30)
//{
//    run();
//}

///////////FULCRUM

//using setup_fulcrum_count_inner =
//    operations_benchmark_inner<
//    kodo_fulcrum::fulcrum_encoder<fifi::binary8>,
//    kodo_fulcrum::fulcrum_inner_decoder<fifi::binary>,
//	kodo_fulcrum::fulcrum_inner_decoder<fifi::binary>>;
//
//BENCHMARK_F_INLINE(setup_fulcrum_count_inner, FulcrumInner, Binary, 30)
//{
//    run();
//}

//using setup_fulcrum_outer =
//    fulcrum_transmissions_benchmark_multihop<
//    kodo_fulcrum::fulcrum_encoder<fifi::binary8>,
//    kodo_rlnc::full_vector_recoder<fifi::binary>,
//	kodo_fulcrum::fulcrum_outer_decoder<fifi::binary8>>;
//
//BENCHMARK_F_INLINE(setup_fulcrum_outer, FulcrumOuter, Binary8, 30)
//{
//    run();
//}

//using setup_fulcrum_combined =
//    fulcrum_transmissions_benchmark_multihop<
//    kodo_fulcrum::fulcrum_encoder<fifi::binary8>,
//    kodo_rlnc::full_vector_recoder<fifi::binary>,
//	fulcrum_combined_decoder<kodo_fulcrum::fulcrum_combined_decoder<fifi::binary8>>>;
//
//BENCHMARK_F_INLINE(setup_fulcrum_combined, FulcrumCombined, Binary8, 30)
//{
//    run();
//}

/////DSEP//////////////

//using setup_dsep_outer =
//    dsep_transmissions_benchmark_multihop<
//    kodo_fulcrum::tunable_fulcrum_encoder<fifi::binary8>,
//    kodo_rlnc::full_vector_recoder<fifi::binary>,
//	kodo_fulcrum::fulcrum_outer_decoder<fifi::binary8>>;
//
//BENCHMARK_F_INLINE(setup_dsep_outer, FulcrumOuter, Binary8, 30)
//{
//    run();
//}
//
//using setup_dsep_combined =
//    dsep_transmissions_benchmark_multihop<
//    kodo_fulcrum::tunable_fulcrum_encoder<fifi::binary8>,
//    kodo_rlnc::full_vector_recoder<fifi::binary>,
//	fulcrum_combined_decoder<kodo_fulcrum::fulcrum_combined_decoder<fifi::binary8>>>;
//
//BENCHMARK_F_INLINE(setup_dsep_combined, FulcrumCombined, Binary8, 30)
//{
//    run();
//}

int main(int argc, const char* argv[])
{
    srand(static_cast<uint32_t>(time(0)));

    gauge::runner::add_default_printers();
    gauge::runner::run_benchmarks(argc, argv);

    return 0;
}
