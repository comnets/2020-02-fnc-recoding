// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <cstdint>
#include <cassert>
#include <vector>
#include <string>
#include <type_traits>

#include <gauge/gauge.hpp>
#include <tables/table.hpp>

#include <ctime>
#include <memory>

#include <boost/random/bernoulli_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <fifi/is_prime2325.hpp>
#include <fifi/prime2325_binary_search.hpp>
#include <fifi/prime2325_apply_prefix.hpp>

#include <kodo_core/has_deep_symbol_storage.hpp>
#include <kodo_core/has_set_mutable_symbols.hpp>
#include <kodo_core/set_systematic_off.hpp>
#include <kodo_core/set_mutable_symbols.hpp>
#include <kodo_core/read_payloads.hpp>
#include <kodo_core/write_payloads.hpp>

/// A test block represents an encoder and decoder pair
template<class Encoder, class Recoder, class Decoder>
struct sparse_rlnc_transmissions_benchmark_multihop : public gauge::benchmark
{
    using encoder_factory = typename Encoder::factory;
    using encoder_ptr = typename Encoder::factory::pointer;

    using recoder_factory = typename Recoder::factory;
    using recoder_ptr = typename Recoder::factory::pointer;

    using decoder_factory = typename Decoder::factory;
    using decoder_ptr = typename Decoder::factory::pointer;

    sparse_rlnc_transmissions_benchmark_multihop(){
        // Seed the random generator controlling the erasures
        m_random_generator.seed((uint32_t)time(0));
        num_relays = 1;
    }

    void start()
    {

    }

    void stop()
    {

    }


    void store_run(tables::table& results)
    {
        if (!results.has_column("num_trans"))
            results.add_column("num_trans");
        results.set_value("num_trans", num_trans);

        if (!results.has_column("num_ldp"))
            results.add_column("num_ldp");
        results.set_value("num_ldp", num_ldp);

        if (!results.has_column("used"))
            results.add_column("used");
        results.set_value("used", m_used);

    }

    std::string unit_text() const
    {
        return "symbols";
    }

    void get_options(gauge::po::variables_map& options)
    {
        auto symbols = options["symbols"].as<std::vector<uint32_t>>();
        auto symbol_size = options["symbol_size"].as<std::vector<uint32_t>>();
        auto types = options["type"].as<std::vector<std::string>>();
        auto erasure1 = options["erasure1"].as<std::vector<double> >();
        auto erasure2 = options["erasure2"].as<std::vector<double> >();
        auto num_relays = options["num_relays"].as<std::vector<uint32_t>>();

        auto density = options["density"].as<std::vector<double>>();

        auto recoding = options["recoding"].as<std::vector<bool>>();

        auto recodingsymbols = options["recoding_symbols"].as<std::vector<uint32_t>>();


        assert(symbols.size() > 0);
        assert(symbol_size.size() > 0);
        assert(types.size() > 0);
        assert(erasure1.size() > 0);
        assert(erasure2.size() > 0);
        assert(num_relays.size() > 0);
        assert(density.size() > 0);
        assert(recoding.size() > 0);
        assert(recodingsymbols.size() > 0);

        for (uint32_t i = 0; i < symbols.size(); ++i)
        {
            for (uint32_t j = 0; j < symbol_size.size(); ++j)
            {
                for (uint32_t u = 0; u < types.size(); ++u)
                {
                	for (const auto& den : density)
					{
						for (const auto& relay : num_relays)
						{
							for(const auto& recsym: recodingsymbols)
							{
							for (const auto& e1 : erasure1)
							{
								for (const auto& e2 : erasure2)
								{
									for (const auto& rec: recoding)
									{

										gauge::config_set cs;
										cs.set_value<uint32_t>("symbols", symbols[i]);
										cs.set_value<uint32_t>("symbol_size", symbol_size[j]);
										cs.set_value<std::string>("type", types[u]);
										cs.set_value<double>("erasure1", e1);
										cs.set_value<double>("erasure2", e2);
										cs.set_value<double>("density", den);
										cs.set_value<uint32_t>("num_relays", relay);
										cs.set_value<bool>("recoding", rec);
										cs.set_value<uint32_t>("recoding_symbols", recsym);
										add_configuration(cs);
									}
								}
							}
						}
						}
					}
                }
            }
        }
    }

    void setup()
    {
	//std::cout<<"BASE SETUP \n";

        gauge::config_set cs = get_current_configuration();

        uint32_t symbols = cs.get_value<uint32_t>("symbols");
        uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");
        erasure1 = cs.get_value<double>("erasure1");
        erasure2 = cs.get_value<double>("erasure2");

        double density = cs.get_value<double>("density");

        num_relays = cs.get_value<uint32_t>("num_relays");

        recoding_symbols = cs.get_value<uint32_t>("recoding_symbols");


        m_distribution1 = boost::random::bernoulli_distribution<>(erasure1);
        m_distribution2 = boost::random::bernoulli_distribution<>(erasure2);


        // Make the factories fit perfectly otherwise there seems to
        // be problems with memory access i.e. when using a factory
        // with max symbols 1024 with a symbols 16
        m_decoder_factory = std::make_shared<decoder_factory>(
            symbols, symbol_size);

        m_encoder_factory = std::make_shared<encoder_factory>(
            symbols, symbol_size);

        //set the recoding matrix (n+r)x(n +r)
        m_recoder_factory = std::make_shared<recoder_factory>(
        	symbols, symbol_size);

        m_recoder_factory->set_recoder_symbols(recoding_symbols);

        setup_factories();

        m_encoder = m_encoder_factory->build();

        m_encoder->set_density(density);

        m_decoder = m_decoder_factory->build();

        m_relays.resize(num_relays);

        for(uint32_t i = 0; i< num_relays; ++i)
        {
        	recoder_ptr recoder = m_recoder_factory->build();
        	m_relays[i] = recoder;
        }


        // Prepare the data buffers
        m_data_in.resize(m_encoder->block_size());
        m_data_out.resize(m_encoder->block_size());

        std::generate_n(m_data_in.begin(), m_data_in.size(), rand);

        // Make sure that the encoder and decoder payload sizes are equal
        assert(m_encoder->payload_size() == m_decoder->payload_size());

        //assert(m_recoder->payload_size() == m_decoder->payload_size());

        // Create the payload buffer
        uint32_t payload_size = m_encoder->payload_size();

        m_temp_payload.resize(payload_size);
        //NVu
        m_recoder_temp_payload.resize(payload_size);

         //payload resize: this payload is used for multi-hops
        payload.resize(m_encoder->payload_size(), 0);

        m_encoder->set_const_symbols(storage::storage(m_data_in));


        m_used = 0;
        num_trans = 0;
        num_ldp = 0;


    }

    /// Setup the factories - we re-factored this into its own function
    /// since some of the codecs like sparse, perpetual and fulcrum
    /// customize the setup of the factory.
    virtual void setup_factories()
    {
    	//std::cout<<"BASE FACTORY \n";
        //gauge::config_set cs = get_current_configuration();

        //uint32_t symbols = cs.get_value<uint32_t>("symbols");
        //set the number of recoding symbols in the case of using full rlnc vector recoder
        //m_recoder_factory->set_recoder_symbols(recoding_symbols);
    }

    /// Where the actual measurement is performed
    void run()
    {

    	gauge::config_set cs = get_current_configuration();
		bool recoding = cs.get_value<bool>("recoding");

        // We switch any systematic operations off so we code
        // symbols from the beginning
        if (kodo_core::has_set_systematic_off<Encoder>::value)
            kodo_core::set_systematic_off(*m_encoder);


        RUN
        {
        	uint32_t pre_rank = 0;

            while (!m_decoder->is_complete())
            {

            	starting:

            	num_trans++;
                // Encode a packet into the payload buffer
                m_encoder->write_payload(payload.data());

                for(uint32_t i = 0; i < num_relays; i++)
				{
					recoder_ptr m_recoder = m_relays[i];

					if (recoding == false)//forwarding
					{
						if (m_distribution1(m_random_generator)) {
							goto starting;
						}

						m_recoder->read_payload(payload.data());
						m_recoder->write_payload(payload.data());

					}
					else//recoding
					{
						if (!m_distribution1(m_random_generator)) {
							m_recoder->read_payload(payload.data());
						}
						m_recoder->write_payload(payload.data());
					}
				}
                //last link
				//if there is no loss on the last link
				if (!m_distribution2(m_random_generator)) {

					m_decoder->read_payload(payload.data());

					if(m_decoder->rank() == pre_rank)
						num_ldp += 1;
					pre_rank = m_decoder->rank();

					m_used++;
				}

            }

        }
    }

protected:

    /// The decoder factory
    std::shared_ptr<decoder_factory> m_decoder_factory;

    /// The encoder factory
    std::shared_ptr<encoder_factory> m_encoder_factory;

    /// The recoder factory
    std::shared_ptr<recoder_factory> m_recoder_factory;

    /// The encoder to use
    encoder_ptr m_encoder;

    /// The number of encoded symbols
    uint32_t m_encoded_symbols;

    /// The number of recoded symbols
    uint32_t m_recoded_symbols;

    /// The decoder to use
    decoder_ptr m_decoder;

    recoder_ptr m_recoder;

    /// List of relays
    std::vector<recoder_ptr> m_relays;

    /// The number of symbols recovered by the decoder
    uint32_t m_recovered_symbols;

    /// The number of symbols processed by the decoder
    uint32_t m_processed_symbols;

    /// The input data
    std::vector<uint8_t> m_data_in;

    /// The output data
    std::vector<uint8_t> m_data_out;

    /// Buffer storing the encoded payloads before adding them to the
    /// decoder. This is necessary since the decoder will "work on"
    /// the encoded payloads directly. Therefore if we want to be able
    /// to run multiple iterations with the same encoded paylaods we
    /// have to copy them before injecting them into the decoder. This
    /// of course has a negative impact on the decoding throughput.
    std::vector<uint8_t> m_temp_payload;

    //NVu
    std::vector<uint8_t> m_recoder_temp_payload;
    std::vector<uint8_t*> m_recoder_payloads;

    /// Contiguous buffer for coded payloads
    std::vector<uint8_t> m_payload_buffer;

    std::vector<uint8_t> m_recoder_payload_buffer;

    /// Pointers to each payload in the payload buffer
    std::vector<uint8_t*> m_payloads;

    std::vector<uint8_t> payload;

    /// Multiplication factor for payload_count
    uint32_t m_factor;

    /// Prefix used when testing with the prime2325 finite field
    uint32_t m_prefix;

    boost::random::mt19937 m_random_generator;

    boost::random::bernoulli_distribution<> m_distribution1;


    boost::random::bernoulli_distribution<> m_distribution2;

    double erasure1;
    double erasure2;

    uint32_t recoder_payload_count = 0;

    uint32_t m_encoder_transmission = 0;
    uint32_t m_recoder_transmission = 0;
    uint32_t m_used = 0, num_trans = 0, num_ldp = 0, recoding_symbols = 3;
    uint32_t num_relays = 1;

    double m_time;


};
