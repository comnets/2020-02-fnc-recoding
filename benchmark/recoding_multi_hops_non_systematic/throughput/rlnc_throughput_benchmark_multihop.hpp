// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <cstdint>
#include <cassert>
#include <vector>
#include <string>
#include <type_traits>

#include <gauge/gauge.hpp>
#include <tables/table.hpp>

#include <ctime>
#include <memory>

#include <boost/random/bernoulli_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <fifi/is_prime2325.hpp>
#include <fifi/prime2325_binary_search.hpp>
#include <fifi/prime2325_apply_prefix.hpp>

#include <kodo_core/has_deep_symbol_storage.hpp>
#include <kodo_core/has_set_mutable_symbols.hpp>
#include <kodo_core/set_systematic_off.hpp>
#include <kodo_core/set_mutable_symbols.hpp>
#include <kodo_core/read_payloads.hpp>
#include <kodo_core/write_payloads.hpp>

/// A test block represents an encoder and decoder pair
template<class Encoder, class Recoder, class Decoder>
struct rlnc_throughput_benchmark_multihop : public gauge::time_benchmark
{
    using encoder_factory = typename Encoder::factory;
    using encoder_ptr = typename Encoder::factory::pointer;

    using recoder_factory = typename Recoder::factory;
    using recoder_ptr = typename Recoder::factory::pointer;

    using decoder_factory = typename Decoder::factory;
    using decoder_ptr = typename Decoder::factory::pointer;

    rlnc_throughput_benchmark_multihop(){
        // Seed the random generator controlling the erasures
        m_random_generator.seed((uint32_t)time(0));
        num_relays = 1;
    }

    void init()
	{
		m_factor = 1;
		gauge::time_benchmark::init();
	}

	void start()
	{
		gauge::time_benchmark::start();
	}

	void stop()
	{
		gauge::time_benchmark::stop();
	}

	double calculate_throughput(bool goodput = false)
	{
		// Get the time spent per iteration
		double time = gauge::time_benchmark::measurement();

		gauge::config_set cs = get_current_configuration();
		std::string type = cs.get_value<std::string>("type");
		//uint32_t symbols = cs.get_value<uint32_t>("symbols");
		uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");

		// The number of bytes {en|de}coded
		uint64_t total_bytes = 0;

		if (type == "decoder")
		{
		   if (goodput)
			   total_bytes = m_recovered_symbols * symbol_size;
		   else
			   total_bytes = m_processed_symbols * symbol_size;
		}
		else if (type == "encoder")
		{
		   total_bytes = m_encoded_symbols * symbol_size;
		}
		else if (type == "recoder")
		{
		   total_bytes = m_recoded_symbols * symbol_size;
		}
		else
		{
		   assert(0 && "Unknown benchmark type");
		}

		// The bytes per iteration
		uint64_t bytes =
		   total_bytes / gauge::time_benchmark::iteration_count();

		return bytes / time; // MB/s for each iteration
	}

  bool accept_measurement()
	{
		gauge::config_set cs = get_current_configuration();

		std::string type = cs.get_value<std::string>("type");

		if (type == "decoder")
		{
			// If we are benchmarking a decoder we only accept
			// the measurement if the decoding was successful
			if (!m_decoder->is_complete())
			{
				// We did not generate enough payloads to decode successfully,
				// so we will generate more payloads for next run
				++m_factor;

				return false;
			}

			// At this point, the output data should be equal to the input
			// data
			assert(m_data_out == m_data_in);
		}

		return gauge::time_benchmark::accept_measurement();
	}

    void store_run(tables::table& results)
    {
    	if (!results.has_column("throughput"))
			results.add_column("throughput");
    	results.set_value("throughput", calculate_throughput(false));

		if (!results.has_column("goodput"))
			results.add_column("goodput");
		results.set_value("goodput", calculate_throughput(true));

        if (!results.has_column("num_trans"))
            results.add_column("num_trans");
        results.set_value("num_trans", num_trans);

        if (!results.has_column("used"))
            results.add_column("used");
        results.set_value("used", m_used);

    }

    std::string unit_text() const
    {
    	return "MB/s";
    }

    void get_options(gauge::po::variables_map& options)
    {
        auto symbols = options["symbols"].as<std::vector<uint32_t>>();
        auto symbol_size = options["symbol_size"].as<std::vector<uint32_t>>();
        auto types = options["type"].as<std::vector<std::string>>();
        auto erasure1 = options["erasure1"].as<std::vector<double> >();
        auto erasure2 = options["erasure2"].as<std::vector<double> >();
        auto num_relays = options["num_relays"].as<std::vector<uint32_t>>();

        auto recodingsymbols = options["recoding_symbols"].as<std::vector<uint32_t>>();

        auto recoding = options["recoding"].as<std::vector<bool>>();


        assert(symbols.size() > 0);
        assert(symbol_size.size() > 0);
        assert(types.size() > 0);
        assert(erasure1.size() > 0);
        assert(erasure2.size() > 0);
        assert(num_relays.size() > 0);
        assert(recoding.size() > 0);
        assert(recodingsymbols.size() > 0);

        for (uint32_t i = 0; i < symbols.size(); ++i)
        {
            for (uint32_t j = 0; j < symbol_size.size(); ++j)
            {
                for (uint32_t u = 0; u < types.size(); ++u)
                {
						for (const auto& relay : num_relays)
						{
							for(const auto& recsym: recodingsymbols)
							{
							for (const auto& e1 : erasure1)
							{
								for (const auto& e2 : erasure2)
								{
									for (const auto& rec: recoding)
									{

										gauge::config_set cs;
										cs.set_value<uint32_t>("symbols", symbols[i]);
										cs.set_value<uint32_t>("symbol_size", symbol_size[j]);
										cs.set_value<std::string>("type", types[u]);
										cs.set_value<double>("erasure1", e1);
										cs.set_value<double>("erasure2", e2);
										cs.set_value<uint32_t>("num_relays", relay);
										cs.set_value<bool>("recoding", rec);

										cs.set_value<uint32_t>("recoding_symbols", recsym);

										add_configuration(cs);
									}
								}
							}
						}
					}

                }
            }
        }
    }

    void setup()
    {
	//std::cout<<"BASE SETUP \n";

        gauge::config_set cs = get_current_configuration();

        uint32_t symbols = cs.get_value<uint32_t>("symbols");
        uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");
        erasure1 = cs.get_value<double>("erasure1");
        erasure2 = cs.get_value<double>("erasure2");
        num_relays = cs.get_value<uint32_t>("num_relays");

        recoding_symbols = cs.get_value<uint32_t>("recoding_symbols");

        m_distribution1 = boost::random::bernoulli_distribution<>(erasure1);
        m_distribution2 = boost::random::bernoulli_distribution<>(erasure2);


        // Make the factories fit perfectly otherwise there seems to
        // be problems with memory access i.e. when using a factory
        // with max symbols 1024 with a symbols 16
        m_decoder_factory = std::make_shared<decoder_factory>(
            symbols, symbol_size);

        m_encoder_factory = std::make_shared<encoder_factory>(
            symbols, symbol_size);

        //set the recoding matrix (n+r)x(n +r)
        m_recoder_factory = std::make_shared<recoder_factory>(
        	symbols, symbol_size);

        m_recoder_factory->set_recoder_symbols(recoding_symbols);

        setup_factories();

        m_encoder = m_encoder_factory->build();
        m_decoder = m_decoder_factory->build();

        m_relays.resize(num_relays);

        for(uint32_t i = 0; i< num_relays; ++i)
        {
        	recoder_ptr recoder = m_recoder_factory->build();
        	m_relays[i] = recoder;
        }


        // Prepare the data buffers
        m_data_in.resize(m_encoder->block_size());
        m_data_out.resize(m_encoder->block_size());

        std::generate_n(m_data_in.begin(), m_data_in.size(), rand);

        // Make sure that the encoder and decoder payload sizes are equal
        assert(m_encoder->payload_size() == m_decoder->payload_size());

        //assert(m_recoder->payload_size() == m_decoder->payload_size());

        // Create the payload buffer
        uint32_t payload_size = m_encoder->payload_size();

        m_temp_payload.resize(payload_size);
        //NVu
        m_recoder_temp_payload.resize(payload_size);

         //payload resize: this payload is used for multi-hops
        payload.resize(m_encoder->payload_size(), 0);

        m_encoder->set_const_symbols(storage::storage(m_data_in));

        // Prepare storage to the encoded payloads
        uint32_t payload_count = symbols * m_factor;
        assert(payload_count > 0);

        // Allocate contiguous payload buffer and store payload pointers
        m_payload_buffer.resize(payload_count * payload_size);
        m_payloads.resize(payload_count);

        //RECODER payload buffer
		m_recoder_payload_buffer.resize(payload_count * payload_size);
		m_recoder_payloads.resize(payload_count);

		for (uint32_t i = 0; i < payload_count; ++i)
		{
		  m_payloads[i] = &m_payload_buffer[i * payload_size];
		  m_recoder_payloads[i] = &m_recoder_payload_buffer[i * payload_size];
		}

        m_used = 0;
        num_trans = 0;
        num_ldp = 0;
        m_processed_symbols = 0;
        m_encoded_symbols = 0;
        m_recoded_symbols = 0;
        recoder_payload_count = 0;



    }

    /// Setup the factories - we re-factored this into its own function
    /// since some of the codecs like sparse, perpetual and fulcrum
    /// customize the setup of the factory.
    virtual void setup_factories()
    {

    }

    /// Where the actual measurement is performed
    void run()
    {
    	gauge::config_set cs = get_current_configuration();
		bool recoding = cs.get_value<bool>("recoding");
        // We switch any systematic operations off so we code
        // symbols from the beginning
        if (kodo_core::has_set_systematic_off<Encoder>::value)
            kodo_core::set_systematic_off(*m_encoder);

        RUN
        {
        	uint32_t pre_rank = 0;
            while (!m_decoder->is_complete())
            {

            	starting:

            	num_trans++;
                // Encode a packet into the payload buffer
                m_encoder->write_payload(payload.data());

                for(uint32_t i = 0; i < num_relays; i++)
				{
					recoder_ptr m_recoder = m_relays[i];

					if (recoding == false)//forwarding
					{
						if (m_distribution1(m_random_generator)) {
							goto starting;
						}

						m_recoder->read_payload(payload.data());
						m_recoder->write_payload(payload.data());

					}
					else//recoding
					{
						if (!m_distribution1(m_random_generator)) {
							m_recoder->read_payload(payload.data());
						}
						m_recoder->write_payload(payload.data());
					}
				}
                //last link
				//if there is no loss on the last link
				if (!m_distribution2(m_random_generator)) {

					m_decoder->read_payload(payload.data());

					if(m_decoder->rank() == pre_rank)
						num_ldp += 1;
					pre_rank = m_decoder->rank();

					m_used++;
				}

            }

        }
    }

    void encode_payloads()
	{
	  // If we are working in the prime2325 finite field we have to
	  // "map" the data first. This cost is included in the encoder
	  // throughput (we do it with the clock running), just as it
	  // would be in a real application.
	  if (fifi::is_prime2325<typename Encoder::field_type>::value)
	  {
		  uint32_t block_length =
			  fifi::size_to_length<fifi::prime2325>(m_encoder->block_size());

		  fifi::prime2325_binary_search search(block_length);
		  m_prefix = search.find_prefix(storage::storage(m_data_in));

		  // Apply the negated prefix
		  fifi::apply_prefix(storage::storage(m_data_in), ~m_prefix);
	  }

	  m_encoder->set_const_symbols(storage::storage(m_data_in));

	  // We switch any systematic operations off so we code
	  // symbols from the beginning
	  if (kodo_core::has_set_systematic_off<Encoder>::value)
		  kodo_core::set_systematic_off(*m_encoder);



		for (uint8_t* payload : m_payloads)
		{
			m_encoder->write_payload(payload);
			++m_encoded_symbols;
			++num_trans;
		}

		  if (fifi::is_prime2325<typename Encoder::field_type>::value)
		  {
			  // Apply the negated prefix
			  fifi::apply_prefix(storage::storage(m_data_in), ~m_prefix);
		  }
		}

    void recode_payloads()
    {
    	recoder_payload_count = 0;
    	uint32_t count = 0;

		for(const uint8_t* payload : m_payloads)
		{
			count++;
			//can not use m_recoder->payload_size() here
			std::copy_n(payload, m_encoder->payload_size(),
			    		                            m_recoder_temp_payload.data());

			for(uint32_t i = 0; i < num_relays; i++)
			{
				recoder_ptr m_recoder;
				m_recoder = m_relays[i];

    			++m_recoder_transmission; //count the number of transmission at recoder

    			//add errors
    			if (m_distribution1(m_random_generator)) {
    				m_recoder->write_payload(m_recoder_temp_payload.data());
    			}
    			else{
    				m_recoder->read_payload(m_recoder_temp_payload.data());
    				m_recoder->write_payload(m_recoder_temp_payload.data());
    			}

//    			m_recoder->read_payload(m_recoder_temp_payload.data());
//
//				m_recoder->write_payload(m_recoder_temp_payload.data());

				//last relay node
				if ( i == num_relays - 1)
				{
					//std::cout<<"recoder count"<<recoder_payload_count<<std::endl;
					uint8_t *p = m_recoder_payloads[recoder_payload_count];
					std::copy_n(m_recoder_temp_payload.data(), m_recoder->payload_size(), p);
					++recoder_payload_count;
					//std::cout<<"recoder count"<<recoder_payload_count<<std::endl;
				}
    			++m_recoded_symbols;

    		}
			//a last relay gets enough n coded pkts
//			if (recoder_payload_count == m_encoder->symbols())
//			{
//				//std::cout<<"break"<<std::endl;
//				break;
//			}
    	}
//    	std::cout<<"Number of papckets sent:" << count <<std::endl;
    }

    void decode_payloads()
      {
      	//assert(m_used == 0);
          // If the decoder uses shallow storage, we have to initialize
          // its decoding buffer
          if (kodo_core::has_set_mutable_symbols<Decoder>::value)
          {
              kodo_core::set_mutable_symbols(*m_decoder, storage::storage(m_data_out));
          }

		  for (const uint8_t* payload : m_recoder_payloads)
		  {

			if(m_distribution2(m_random_generator))
			{
				continue;
			}
			  std::copy_n(payload, m_decoder->payload_size(),
						  m_temp_payload.data());

			  m_decoder->read_payload(m_temp_payload.data());

			  ++m_processed_symbols;
			  ++m_used;

			  if (m_decoder->is_complete())
			  {
				  break;
			  }
		  }

          if (m_decoder->is_complete())
          {
              // If the decoder uses deep storage, we have to copy
              // its decoding buffers for verification.
              // This would also be necessary in real applications, so it
              // should be part of the timed benchmark.
              if (kodo_core::has_deep_symbol_storage<Decoder>::value)
              {
                  m_decoder->copy_from_symbols(storage::storage(m_data_out)); //be performed
              }

              if (fifi::is_prime2325<typename Decoder::field_type>::value)
              {
                  // Now we have to apply the negated prefix to the decoded data
                  fifi::apply_prefix(storage::storage(m_data_out), ~m_prefix);
              }

              m_recovered_symbols += m_decoder->symbols();

          }


      }

    /// Run the encoder
    void run_encode()
    {
        // The clock is running
        RUN
        {
            // We have to make sure the encoder is in a "clean" state
            m_encoder->initialize(*m_encoder_factory);

            encode_payloads();
        }
    }

    /// Run the recoder
    void run_recode()
    {
    	encode_payloads();
        // The clock is running
        RUN
        {
            // We have to make sure the encoder is in a "clean" state
            for(uint32_t i = 0; i < num_relays; i++){
            	m_relays[i]->initialize(*m_recoder_factory);
            }

            recode_payloads();
        }
    }

    /// Run the decoder
    void run_decode()
    {
        // Encode some data
        encode_payloads();

        // Recode data
        recode_payloads();

        // Zero the data buffer for the decoder
        std::fill_n(m_data_out.begin(), m_data_out.size(), 0);

        // The clock is running
        RUN
        {
            // We have to make sure the decoder is in a "clean" state
            // i.e. no symbols already decoded.
            m_decoder->initialize(*m_decoder_factory);

            // Decode the payloads
            decode_payloads();
        }
    }

    void run_body()
    {
        gauge::config_set cs = get_current_configuration();

        std::string type = cs.get_value<std::string>("type");

        if (type == "encoder")
        {
            run_encode();
        }
        else if (type == "recoder")
        {
            run_recode();
        }
        else if (type == "decoder")
        {
            run_decode();
        }
        else
        {
            assert(0 && "Unknown benchmark type");
        }
    }

protected:

    /// The decoder factory
    std::shared_ptr<decoder_factory> m_decoder_factory;

    /// The encoder factory
    std::shared_ptr<encoder_factory> m_encoder_factory;

    /// The recoder factory
    std::shared_ptr<recoder_factory> m_recoder_factory;

    /// The encoder to use
    encoder_ptr m_encoder;

    /// The number of encoded symbols
    uint32_t m_encoded_symbols;

    /// The number of recoded symbols
    uint32_t m_recoded_symbols;

    /// The decoder to use
    decoder_ptr m_decoder;

    recoder_ptr m_recoder;

    /// List of relays
    std::vector<recoder_ptr> m_relays;

    /// The number of symbols recovered by the decoder
    uint32_t m_recovered_symbols;

    /// The number of symbols processed by the decoder
    uint32_t m_processed_symbols;

    /// The input data
    std::vector<uint8_t> m_data_in;

    /// The output data
    std::vector<uint8_t> m_data_out;

    /// Buffer storing the encoded payloads before adding them to the
    /// decoder. This is necessary since the decoder will "work on"
    /// the encoded payloads directly. Therefore if we want to be able
    /// to run multiple iterations with the same encoded paylaods we
    /// have to copy them before injecting them into the decoder. This
    /// of course has a negative impact on the decoding throughput.
    std::vector<uint8_t> m_temp_payload;

    //NVu
    std::vector<uint8_t> m_recoder_temp_payload;
    std::vector<uint8_t*> m_recoder_payloads;

    /// Contiguous buffer for coded payloads
    std::vector<uint8_t> m_payload_buffer;

    std::vector<uint8_t> m_recoder_payload_buffer;

    /// Pointers to each payload in the payload buffer
    std::vector<uint8_t*> m_payloads;

    std::vector<uint8_t> payload;

    /// Multiplication factor for payload_count
    uint32_t m_factor;

    /// Prefix used when testing with the prime2325 finite field
    uint32_t m_prefix;

    boost::random::mt19937 m_random_generator;

    boost::random::bernoulli_distribution<> m_distribution1;


    boost::random::bernoulli_distribution<> m_distribution2;

    double erasure1;
    double erasure2;

    uint32_t recoder_payload_count = 0;

    uint32_t m_encoder_transmission = 0;
    uint32_t m_recoder_transmission = 0;
    uint32_t m_used = 0, num_trans = 0, num_ldp = 0, recoding_symbols = 3;
    uint32_t num_relays = 1;

    double m_time;


};
