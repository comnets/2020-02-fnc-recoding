/*
 * time_measurement.hpp
 *
 *  Created on: Mar 28, 2018
 *      Author: nguyenvu
 */
#ifndef TIME_MEASUREMENT_HPP_
#define TIME_MEASUREMENT_HPP_

#include <boost/chrono.hpp>
#include <algorithm>
namespace bc =boost::chrono;
struct time_measurement
{
	///start time
	bc::high_resolution_clock::time_point m_start;
	///stop time
	bc::high_resolution_clock::time_point m_stop;
	///result in microseconds
	double m_result = 0;
	uint32_t count = 0;
	std::vector<double> multi_times;
	public:
	void init()
	{
		m_result = 0;
		count = 0;
		multi_times.clear();

	}
	void start()
	{
		count++;
		m_start = bc::high_resolution_clock::now();
	}
	void stop()
	{
		m_stop = bc::high_resolution_clock::now();
		m_result = static_cast<double>(bc::duration_cast<bc::nanoseconds>(m_stop-m_start).count());
		multi_times.push_back(m_result);
	}
	double measurement()
	{
		return m_result;
	}
	double get_sum_time()
	{
		double sum = 0;
		for (uint32_t i = 0; i < multi_times.size(); i++ )
			sum += multi_times[i];
		return sum;
	}


};
#endif
