// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <kodo_core/generator_layers.hpp>
#include <kodo_core/seed_generator.hpp>
#include "adaptive_recoder_uniform_generator.hpp"

namespace kodo_core
{
/// @ingroup features_coefficient_generator_variants
///
/// Specialization for the uniform generator strategy. See
/// generator_layers.hpp for more detailed information.
///
/// To define a custom generator API, simply create a new struct inheriting
/// from generator_layers which cotains an inner type as shown below with
/// with the required layers.
///
struct adaptive_recoder_uniform_generator_layers : generator_layers
{
    template<class Features, class SuperCoder>
    using type =
        seed_generator<
        adaptive_recoder_uniform_generator<SuperCoder>>;
};
}
