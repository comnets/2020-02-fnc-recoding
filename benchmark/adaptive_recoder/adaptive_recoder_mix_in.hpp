// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <cstdint>
#include <vector>
#include <cassert>

#include <storage/storage.hpp>

namespace kodo_rlnc
{
/// @ingroup decoder_layers
///
/// @brief Uses the internal recoders to store and recodes the incoming and
///        outgoing data.
///        Two inner recoders are used, one for the symbol data and one for
///        the coefficient vector.
///        The data is recoded using common coefficient vector generated
///        based on the symbols read by the internal recoders.
template<class SuperCoder>
class adaptive_recoder_mix_in : public SuperCoder
{
public:

    /// @copydoc layer::initialize(Factory&)
    template<class Factory>
    void initialize(Factory& the_factory)
    {
        SuperCoder::initialize(the_factory);
        m_coefficients.resize(
            SuperCoder::recoder_coefficient_vector_size());
    }

    /// @copydoc layer::read_symbol(uint8_t*,uint8_t*)
    void read_symbol(uint8_t* symbol_data, uint8_t* coefficients)
    {
        assert(symbol_data != 0);
        assert(coefficients != 0);

        // Generate a mix-in encoding vector
        //SuperCoder::recoder_generate(m_coefficients.data());

        // Recode-in step: Mix the incoming symbol encoding vector into
        //                 the existing encoding vectors.
        uint32_t initialized = SuperCoder::symbols_initialized();

        //std::cout<<"symbols:"<<SuperCoder::recoder_symbols()<<std::endl;

        if (initialized == SuperCoder::recoder_symbols())
        {
        	//std::cout<<"initialized:"<<initialized<<std::endl;

        	uint8_t *temp = m_coefficients.data();
			for(uint32_t i = 0; i < SuperCoder::symbols(); i++)
			{
				SuperCoder::set_coefficient_value(temp, i, 1U);
			}

			for (uint32_t i = 0; i < initialized; ++i)
			{
				assert(SuperCoder::is_symbol_initialized(i) == true);

				uint8_t* current_symbol =
					SuperCoder::mutable_symbol(i);

				uint8_t* current_coefficients =
					SuperCoder::coefficient_vector_data(i);

				uint8_t* next_symbol;// = SuperCoder::mutable_symbol(i+1);
				uint8_t* next_coefficients;// = SuperCoder::coefficient_vector_data(i+1);
				//add the incoming data to the last element
				if (i == initialized - 1){
					next_symbol = symbol_data;
					next_coefficients = coefficients;
				}
				else{
					next_symbol = SuperCoder::mutable_symbol(i+1);
					next_coefficients = SuperCoder::coefficient_vector_data(i+1);
				}

				//empty the current symbol and coefficients data
				std::fill_n(current_symbol, SuperCoder::symbol_size(), 0);
				std::fill_n(current_coefficients, SuperCoder::coefficient_vector_size(), 0);

				auto coefficient =
					SuperCoder::coefficient_value(m_coefficients.data(), i);

				SuperCoder::multiply_add(current_symbol, next_symbol,
										 coefficient, SuperCoder::symbol_size());

				SuperCoder::multiply_add(
					current_coefficients, next_coefficients, coefficient,
					SuperCoder::coefficient_vector_size());
			}
        }

        // If the encoder is full (i.e. no unused space) we are done.
        if (SuperCoder::is_symbols_initialized())
            return;

        // If we are not full copy this symbol to the next empty
        // location
        uint32_t index = SuperCoder::symbols_initialized();

        assert(SuperCoder::symbols() > index);
        assert(SuperCoder::is_symbol_initialized(index) == false);

        auto symbol_storage = storage::storage(symbol_data,
                                               SuperCoder::symbol_size());

        auto coefficients_storage =
            storage::storage(
                coefficients, SuperCoder::coefficient_vector_size());

        SuperCoder::set_const_symbol(index, symbol_storage);

        /// @todo Remove data and add const on this function
        SuperCoder::set_coefficient_vector_data(index, coefficients_storage);
    }

private:

    /// Buffer for storing the generated coefficients.
    std::vector<uint8_t> m_coefficients;
};
}
