// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include "on_the_fly_adaptive_recoder.hpp"

namespace kodo_rlnc
{
/// @brief Define the stack type used for recording
template<class Features, class SuperCoder>
class on_the_fly_adaptive_recoder_type : public SuperCoder
{
public:

    /// @copydoc layer::field_type
    using field_type = typename SuperCoder::field_type;

    /// Uses the on_the_fly_encoder for the recoding.
    using recoder_type = on_the_fly_adaptive_recoder<field_type, Features>;
};
}
