// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <kodo_core/check_partial_generator.hpp>
#include <adaptive_recoder/adaptive_recoder_uniform_generator_layers.hpp>
#include <kodo_core/pivot_aware_generator.hpp>

namespace kodo_rlnc
{
/// @ingroup coefficient_generator_layers
///
/// @brief Generates an uniform random coefficient (from the
///        chosen Finite Field) for every symbol. In addition
///        using the pivot_aware_generate means that we will only
///        generate non-zero coefficients for symbols which are
///        available locally.
template<class Features, class SuperCoder>
using adaptive_recoder_on_the_fly_generator =
    kodo_core::check_partial_generator<
    kodo_core::adaptive_recoder_uniform_generator_layers::type<Features,
    kodo_core::pivot_aware_generator<
    SuperCoder> > >;
}
