// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <cstdint>
#include <cassert>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int_distribution.hpp>

namespace kodo_core
{
/// @ingroup coefficient_generator_layers
///
/// @brief Generates an uniform random coefficient (from the chosen
///        Finite Field) for every symbol.
///
template<class SuperCoder>
class adaptive_recoder_uniform_generator : public SuperCoder
{
public:

    /// @copydoc layer::value_type
    using value_type = typename SuperCoder::value_type;

    /// The random generator used
    using generator_type = boost::random::rand48;

    /// @copydoc layer::seed_type
    using seed_type = generator_type::result_type;

public:

    /// Constructor
    adaptive_recoder_uniform_generator() :
        m_distribution(),
        m_value_distribution(
            SuperCoder::field::min_value(),
            SuperCoder::field::max_value())
    { }

    /// @copydoc layer::generate(uint8_t*)
    void generate(uint8_t* coefficients)
    {
        assert(coefficients != 0);

//        uint32_t size = SuperCoder::coefficient_vector_size();
//        std::cout<<"size:"<<size<<std::endl;
//        for (uint32_t i = 0; i < size; ++i)
//        {
//            coefficients[i] = m_distribution(m_random_generator);
//        }

        ///Vu: change to partitial generator
        std::fill_n(coefficients, SuperCoder::coefficient_vector_size(), 0);

	   uint32_t symbols = SuperCoder::symbols();

	   for (uint32_t i = 0; i < symbols - 1 ; i++)
		{
			//std::cout<<"i = "<<i<<std::endl;
			if (!SuperCoder::can_generate(i))
			{
				continue;
			}
			//make sure last coded symbols is combined
			if (!SuperCoder::can_generate(i+1))
			{
				SuperCoder::set_coefficient_value(coefficients, i, 1U);
				//std::cout<<"Set 1 at i = "<<i<<std::endl;
			}
			else
			{
				value_type coefficient = m_value_distribution(m_random_generator);
				SuperCoder::set_coefficient_value(coefficients, i, coefficient);
			}
		}
		if (SuperCoder::can_generate(symbols - 1))
		{
			SuperCoder::set_coefficient_value(coefficients, symbols - 1, 1U);
			//std::cout<<"Set 1 at last i = "<<symbols - 1<<std::endl;
		}

    }

    /// @copydoc layer::generate_partial(uint8_t*)
    void generate_partial(uint8_t* coefficients)
    {
        assert(coefficients != 0);

        // Since we will not set all coefficients we should ensure
        // that the non specified ones are zero
        std::fill_n(coefficients, SuperCoder::coefficient_vector_size(), 0);

        uint32_t symbols = SuperCoder::symbols();

        for (uint32_t i = 0; i < symbols - 1 ; i++)
			{
				//std::cout<<"i = "<<i<<std::endl;
				if (!SuperCoder::can_generate(i))
				{
					continue;
				}
				//make sure last coded symbols is combined
				if (!SuperCoder::can_generate(i+1))
				{
					SuperCoder::set_coefficient_value(coefficients, i, 1U);
					//std::cout<<"Set 1 at i = "<<i<<std::endl;
				}
				else
				{
					value_type coefficient = m_value_distribution(m_random_generator);
					SuperCoder::set_coefficient_value(coefficients, i, coefficient);
				}
			}
			if (SuperCoder::can_generate(symbols - 1))
			{
				SuperCoder::set_coefficient_value(coefficients, symbols - 1, 1U);
				//std::cout<<"Set 1 at last i = "<<symbols - 1<<std::endl;
			}
    }

    /// @copydoc layer::set_seed(seed_type)
    void set_seed(seed_type seed_value)
    {
        m_random_generator.seed(seed_value);
    }

private:
    /// The random generator
    generator_type m_random_generator;

    /// The type of the uint8_t distribution
    using uint8_t_distribution =
        boost::random::uniform_int_distribution<uint8_t>;

    /// Distribution that generates random bytes
    uint8_t_distribution m_distribution;

    /// The type of the value_type distribution
    using value_type_distribution =
        boost::random::uniform_int_distribution<value_type>;

    /// Distribution that generates random values from a finite field
    value_type_distribution m_value_distribution;
};
}
