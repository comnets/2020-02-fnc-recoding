// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <kodo_core/coefficient_info.hpp>
#include <kodo_core/coefficient_value_access.hpp>
#include <kodo_core/common_encoder_layers.hpp>
#include <kodo_core/deep_storage_layers.hpp>
#include <kodo_core/default_on_systematic_encoder.hpp>
#include <kodo_core/final_layer.hpp>
#include <kodo_core/find_enable_trace.hpp>
#include <kodo_core/finite_field_layers.hpp>
#include <kodo_core/linear_block_encoder.hpp>
#include <kodo_core/no_locking_policy.hpp>
#include <kodo_core/payload_info.hpp>
#include <kodo_core/payload_rank_encoder.hpp>
#include <kodo_core/plain_symbol_id_size.hpp>
#include <kodo_core/plain_symbol_id_writer.hpp>
#include <kodo_core/rank_info.hpp>
#include <kodo_core/select_locking_policy.hpp>
#include <kodo_core/select_storage_layers.hpp>
#include <kodo_core/storage_aware_encoder.hpp>
#include <kodo_core/symbol_id_encoder.hpp>
#include <kodo_core/trace_layer.hpp>
#include <kodo_core/write_symbol_tracker.hpp>
#include <kodo_core/zero_symbol_encoder.hpp>

//#include "on_the_fly_generator.hpp"
#include "adaptive_recoder_on_the_fly_generator.hpp"

namespace kodo_rlnc
{
/// @ingroup fec_stacks
///
/// @brief Complete stack implementing a full RLNC on-the-fly encoder.
///
/// The on-the-fly encoder has the advantage that symbols can be
/// specified as they arrive at the encoder. This breaks with a
/// traditional block code where all the data has to be available
/// before encoding can start.
///
/// Implementation of on the fly RLNC encoder uses a storage aware
/// generator and storage aware encoder.  The storage aware
/// generator makes sure that we do not generate non-zero
/// coefficients for the missing symbols, the storage aware
/// encoder provides the generator with information about how many
/// symbols have been specified.
template
<
    class Field,
    class Features = meta::typelist<>
>
class on_the_fly_adaptive_recoder : public
    // Payload Codec API
    kodo_core::payload_rank_encoder<
    kodo_core::payload_info<
    // Codec Header API
    kodo_core::default_on_systematic_encoder<
    kodo_core::symbol_id_encoder<
    // Symbol ID API
    kodo_core::plain_symbol_id_writer<
    kodo_core::plain_symbol_id_size<
    // Coefficient Generator API
    //original version
    //on_the_fly_generator<Features,
    adaptive_recoder_on_the_fly_generator<Features,
    // Encoder API
    kodo_core::common_encoder_layers<Features,
    kodo_core::rank_info<
    // Coefficient Storage API
    kodo_core::coefficient_value_access<
    kodo_core::coefficient_info<
    // Symbol Storage API
    kodo_core::select_storage_layers<
    kodo_core::deep_storage_layers, Features,
    // Finite Field API
    kodo_core::finite_field_layers<Field,
    // Trace Layer
    kodo_core::trace_layer<kodo_core::find_enable_trace<Features>,
    // Final Layer
    kodo_core::final_layer
    > > > > > > > > > > > > > >
{
public:

    using locking_policy = kodo_core::select_locking_policy<
                           kodo_core::no_locking_policy, Features>;

    using factory = kodo_core::pool_factory<on_the_fly_adaptive_recoder, locking_policy>;
};
}
