import matplotlib.pyplot as plt
import numpy as np
import scipy
import csv
import json
import itertools
import pandas as pd
plt.style.use(['seaborn-pastel', 'presentation.mplstyle'])
plt.rcParams['axes.linewidth'] = 0.4
plt.rcParams['grid.linestyle'] = '--'
plt.rcParams['grid.linewidth'] = 0.2
plt.rcParams['axes.labelpad'] = 2
plt.rcParams['hatch.linewidth'] = 0.2
plt.rcParams['hatch.color'] = 'w'

from matplotlib.ticker import FormatStrFormatter

from matplotlib.ticker import NullFormatter  # useful for `logit` scale

path_sources = '../data/'
path_pc = '/home/nguyenvu/dev/kodo-fulcrum-sparse/build/linux/benchmark/tunable_fulcrum_benchmark_journal/throughput_fulcrum/'
path_siso = '/home/nguyenvu/dev/kodo-fulcrum-sparse/build/linux/benchmark/throughput_fulcrum_sparse_2nodes/'

path_destination = '/home/nguyenvu/dev/2020-02-fnc-recoding/docs/figures/for_frank/e1_005_e2_03/'


# erasure = 0.0
count_type = 'num_trans'
#GlobeCome
# width = 3.500
# height = 2.900
width = 3.500
height = 3.3

count_trans_or_used = 'used'


import itertools
def getCM(md):
    """ This function selects a color and marker that can be used in plots"""
    if not md in getCM.saved:
        getCM.saved[md] = next(getCM.colormarker)
    return getCM.saved[md]
getCM.saved = {}
getCM.colormarker = itertools.cycle(zip(
    # Colors
    ('r','c','m','y','#F0A804','#FF6600','#8C5757','#F200FF', '#808080'),
    # Markers
    ('D','^','o','*','s','.','>', 'x', '8', 'v', '1', '<', 'd' ),
    ))
#decoder_type_range =  ['FulcrumCombined', n'FulcrumInner', 'FulcrumOuter']
decoder_type_range =  ['TunableFulcrumOuter']
benchmark_range = ['Binary','Binary8']
markers = ['o', '^', 's', 'p', 'h', '*', 'D', 'v', '>']
marker_list = ['o', '^', 's', 'p',  'd', '*', 'h' , 'v', '>']
colors = ['r','c','m','y','#F0A804','#FF6600','#8C5757','#F200FF', '#808080']
g = itertools.cycle(colors)

colors_merge_de_prob = ['c','m','#F0A804','#FF6600']
g1 = itertools.cycle(colors_merge_de_prob)

def open_json_file(filename, expansion, testcase, symbol, sparse=0, redundant=0, threshold=-1, density=0, erasure=0.0, type='decoder'):

    with open(filename, 'rb') as jsonfile:
        df = pd.read_json(jsonfile)

    df = df[(df['testcase'] == testcase) & (df['symbols'] == symbol) & (df['expansion'] == expansion)
            & (df['erasure'].round(2) == np.around(erasure, 2)) & (df['type'] == type)]


    if sparse > 0:
        df = df[(df['sparse'] == sparse)]

    if redundant > 0:
        df = df[(df['redundant'] == redundant)]

    if threshold >= 0:
        df = df[(df['threshold'] == threshold)]

    if density > 0:
        df = df[(df['density'].round(1) == np.around(density, 1))]



    # total_used = df['used']
    total_used = df[count_trans_or_used]  # Vu used name 'symbols_used'
    iterations = df['iterations']

    t = []
    it = []

    for t in total_used:
        a = 1
    for it in iterations:
        a = 1

    if testcase == 'FulcrumSpadenCombined':
        print np.array(t) / np.array(it)
        print df['density']


    return np.array(t) / np.array(it)


measuretype = 'throughput'


def open_file(filename):
    with open(filename, 'r') as f:
        data = json.load(f)
    data = pd.DataFrame(data)
    return data


def trans_recoding_vs_forwarding():

    bar_width = 0.15
    opacity = 0.7
    N = 7
    # N = 5
    ind = np.arange(N)
    symbols = [16, 32, 64, 128, 256, 512, 1024]
    fulcr_forwarding = [37.90, 73.3, 144.17, 285.54, 571.29, 1140.1, 2277.51]
    fulcr_recoding = [34.57, 66.85, 129.86, 257.89, 514.76, 1024.88, 2053.39]

    dsep_forwarding = [38.04, 73.12, 144.61, 285.68, 571.61, 1140.44, 2279.08]
    dsep_recoding = [34.74, 66.03, 130.10, 258.28, 514.51, 1025.66, 2051.27]

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    ax.bar(ind, fulcr_forwarding[:7], bar_width, alpha=opacity, label='Fulcr. Forwarding',
           lw=0, color='black')
    ax.bar(ind + 1 * bar_width, fulcr_recoding[:7], bar_width, alpha=opacity, label='Fulcr. Recoding',
           lw=0, color='blue')
    ax.bar(ind + 2 * bar_width, dsep_forwarding[:7], bar_width, alpha=opacity,
           label='DSEP Forwarding', lw=0, color='c')
    ax.bar(ind + 3 * bar_width, dsep_recoding[:7], bar_width, alpha=opacity,
           label='DSEP Recoding', lw=0, color='m')
    # if testcase == 'Inner':
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaInner'], bar_width, alpha=opacity,
    #            label='SISO Inner', color=colors[0], lw=0, hatch='/////')
    # elif testcase == 'Outer':
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaOuter'], bar_width, alpha=opacity,
    #            label='SISO Outer', lw=0, color=colors[1], hatch='.....', )
    # else:
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaCombined'], bar_width, alpha=opacity,
    #            label='SISO Combined', lw=0, color=colors[2], hatch='xxxxx')

    plt.xlabel('Generation Size ' + r'$n$')
    plt.ylabel('Number of Transmissions')

    y_scale = [35, 65, 130, 250, 510, 1025, 2050, 2280]
    plt.yscale('log')
    ax.yaxis.set_major_formatter(FormatStrFormatter('%g'))

    plt.yticks(y_scale)
    plt.ylim((34, 2280))

    ax.set_xticks(ind + 2 * bar_width)
    # ax.set_yticks([0, 20, 40, 60, 80, 100, 120, 140])

    # ax.set_yticks(range(0, 250))
    # ax.set_ylim([y_min_lim, y_max_lim])
    # ax.set_xlim([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8])

    ax.set_xticklabels(symbols[:7])
    ax.tick_params(width=0)
    # Hoac: plt.xticks(index + bar_width / 2, ('A', 'B', 'C', 'D', 'E'))

    plt.legend(loc='best')
    plt.grid(True)
    fig.set_size_inches(width, height)
    filename = '../figures/recoding/test.pdf'
    fig.savefig(filename)
    plt.close()

path_to_multihops = '/home/nguyenvu/dev/kodo-fulcrum-sparse/build/linux/benchmark/recoding_multi_hops_non_systematic/'
output_path = '/home/nguyenvu/dev/2020-02-fnc-recoding/docs/figures/'

def get_avg_ldp_recodingsymbols(filename, relay, testcase='FulcrumCombined', expansion=0, redundant=0, density=0.0, symbol=32, e1=0.05, e2=0.3):

    df = open_file(filename)

    df = df[(df['testcase'] == testcase) & (df['symbols'] == symbol) &
            (df['erasure1'].round(2) == np.around(e1, 2)) & (df['erasure2'].round(2) == np.around(e2, 2))]

    if expansion > 0:
        df = df[(df['expansions'] == expansion)]

    if redundant > 0:
        df = df[(df['redundant'] == redundant)]

    if density > 0.0:
        df = df[(df['density'].round(2) == np.around(density, 2))]

    if relay >= 0:
        df = df[(df['num_relays'] == relay)]

    # print df
    recoding_symbols = [3, 6, 10, 12, 16, 20, 24]
    avg_trans = []
    avg_ldp = []
    for recsym in recoding_symbols:
        df1 = df[df['recoding_symbols'] == recsym]
        df2 = df1['num_ldp']
        for v in df2:
            avg_ldp.append(np.mean(v))

        df2 = df1['num_trans']
        for v in df2:
            avg_trans.append(np.mean(v))

    return avg_ldp, avg_trans

def get_avg_ldp_relays(filename, recoding_symbols, testcase='FulcrumCombined', expansion=0, redundant=0, density=0.0, symbol=32, e1=0.1, e2=0.5, recoding=True, relays=[1]):

    df = open_file(filename)

    df = df[(df['testcase'] == testcase) & (df['symbols'] == symbol) &
            (df['erasure1'].round(2) == np.around(e1, 2)) & (df['erasure2'].round(2) == np.around(e2, 2))]

    if expansion > 0:
        df = df[(df['expansions'] == expansion)]

    if redundant > 0:
        df = df[(df['redundant'] == redundant)]



    if density > 0.0:
        df = df[(df['density'].round(2) == np.around(density, 2))]

    if recoding_symbols >= 0:
        df = df[(df['recoding_symbols'] == recoding_symbols)]

    df = df[(df['recoding'] == recoding)]

    print filename

    # Globecom
    #relays  = [0, 2, 4, 6, 8, 10]

    avg_trans = []
    avg_ldp = []
    for relay in relays:
        df1 = df[df['num_relays'] == relay]
        df2 = df1['num_ldp']
        for v in df2:
            avg_ldp.append(np.mean(v))

        df2 = df1['num_trans']
        for v in df2:
            avg_trans.append(np.mean(v))

    return avg_ldp, avg_trans

def get_avg_ldp_expansion(filename, recoding_symbols, testcase='FulcrumCombined', redundant=0, density=0.0, symbol=32, e1=0.1, e2=0.5, recoding=True, relays=1, expansions=[2]):

    df = open_file(filename)

    df = df[(df['testcase'] == testcase) & (df['symbols'] == symbol) &
            (df['erasure1'].round(2) == np.around(e1, 2)) & (df['erasure2'].round(2) == np.around(e2, 2))]


    if redundant > 0:
        df = df[(df['redundant'] == redundant)]

    if density > 0.0:
        df = df[(df['density'].round(2) == np.around(density, 2))]

    if recoding_symbols >= 0:
        df = df[(df['recoding_symbols'] == recoding_symbols)]

    df = df[(df['recoding'] == recoding)]

    df = df[df['num_relays'] == relays]

    avg_trans = []
    avg_ldp = []
    for expansion in expansions:
        if testcase in ['RLNC2', 'RLNC8']:
            df1 = df
        else:
            df1 = df[(df['expansions'] == expansion)]
        df2 = df1['num_ldp']
        for v in df2:
            avg_ldp.append(np.mean(v))

        df2 = df1['num_trans']
        for v in df2:
            avg_trans.append(np.mean(v))

    return avg_ldp, avg_trans

# fulcr_name = path_to_multihops+'transmission/'+'fulcr_combined_symbols32_relays0246810_r2_e1_01_e2_05_recodingsymbols_030220.json'

# dsep_name = path_to_multihops + 'transmission/' + 'dsep_combined_symbols32_relays0246810_r2_e1_01_e2_05_red5_recodingsymbols_030220.json'
# dsep_name = path_to_multihops + 'transmission/' + 'dsep_ad_recoding_symbols32_relays0246810_e1_01_e2_05_r2_red5_recodingsymbols_030220.json'

# rlnc2_name = path_to_multihops + 'transmission/' + 'rlnc2_symbols32_relays0246810_e1_01_e2_05_recodingsymbols_030220.json'
# rlnc8_name = path_to_multihops + 'transmission/' + 'rlnc8_symbols32_relays0246810_e1_01_e2_05_recodingsymbols_030220.json'


# srlnc2_name = path_to_multihops + 'transmission/' + 'sparse_rlnc2_symbols32_relays0246810_e1_01_e2_05_den0103_recodingsymbols_030220.json'
# srlnc8_name = path_to_multihops + 'transmission/' + 'sparse_rlnc8_symbols32_relays0246810_e1_01_e2_05_den010203_recodingsymbols_030220.json'

##orig RLNC recoder

fulcr_name_orig_recoder_rs610 = path_to_multihops + 'transmission/' + 'fulcrum_orig_recoder_sym32_r2_relays010_e1_005_e2_0203_rc_610_240220.json'
dsep_name_orig_recoder_rs610 = path_to_multihops + 'transmission/' + 'dsep_orig_recoder_sym32_r2_red520_relays010_e1_005_e2_0203_rs_610_240220.json'

rlnc_name_orig_recoder_rs610 = path_to_multihops + 'transmission/' + 'rlnc_orig_recoder_sym32_relays01246810_e1_005_e2_0203_rs_610_240220.json'
srlnc_name_orig_recoder_rs610 = path_to_multihops + 'transmission/' + 'sparse_orig_rlnc2_recoder_sym32_relays010_e1_005_e2_0203_rs_610_den0103_240220.json'

fulcr_name_orig_recoder_full = path_to_multihops + 'transmission/' + 'fulcrum_inncomb_orig_recoder_sym32_r2_relays010_e1_005_e2_03_rc_361012162024_240220.json'
dsep_name_orig_recoder_full = path_to_multihops + 'transmission/' + 'dsep_inncomb_orig_recoder_sym32_r2_red520_relays010_e1_005_e2_03_rs_361012162024_240220.json'

rlnc_name_orig_recoder_full = path_to_multihops + 'transmission/' + 'rlnc_orig_recoder_sym32_relays246810_e1_005_e2_03_rs_361012162024_240220.json'
srlnc_name_orig_recoder_full = path_to_multihops + 'transmission/' + 'sparse_orig_rlnc2_recoder_sym32_relays010_e1_005_e2_03_rs_361012162024_den0103_240220.json'


##new data with new adaptive recoder for GF(2) and with recoding/decoding goodput
fulcr_name = path_to_multihops+'transmission/' + 'fulcrum_sym32_r2_relays010_e1_005_e2_0203_rc_23632_240220.json'
dsep_name = path_to_multihops + 'transmission/' + 'dsep_sym32_r2_red520_relays01246810_e1_005_e2_0203_rs_23632_240220.json'

rlnc_name = path_to_multihops + 'transmission/' + 'rlnc_sym32_relays01246810_e1_005_e2_0203_rs_23632_240220.json'
srlnc_name = path_to_multihops + 'transmission/' + 'sparse_rlnc_sym32_relays010_e1_005_e2_0203_rs_23632_den0103_240220.json'

dsep_name_new_recoder_full = path_to_multihops + 'transmission/' + 'dsep_inncomb_new_recoder_sym32_r2_red520_relays010_e1_005_e2_03_rs_361012162024_240220.json'


fulcr_name_1relay = path_to_multihops+'transmission/' + 'fulc_sym32_r2_relays1_e1_005_e2_0203_rs_23632_190220.json'
dsep_name_1relay = path_to_multihops + 'transmission/' + 'dsep_sym32_r2_red520_relays1_e1_005_e2_0203_rs_23632_190220.json'
rlnc_name_1relay = path_to_multihops + 'transmission/' + 'rlnc_sym32_relays1_e1_005_e2_0203_rs_23632_190220.json'

dsep_name_inner = path_to_multihops+'transmission/' + 'dsep_inner_sym32_r2_red5_relays01246810_e1_005_e2_03_rs_332_240220.json'

#forwarding
fulcr_name_forwarding = path_to_multihops+'transmission/' + 'fulcrum_forwarding_sym32_r2_relays010_e1_005_e2_0203_rc_32_120220.json'
dsep_name_forwarding = path_to_multihops + 'transmission/' + 'dsep_forwarding_sym32_r2_red520_relays010_e1_005_e2_0203_rs_32_120220.json'

rlnc_name_forwarding = path_to_multihops + 'transmission/' + 'rlnc_forwarding_sym32_relays010_e1_005_e2_0203_rs_23632_120220.json'
srlnc_name_forwarding = path_to_multihops + 'transmission/' + 'sparse_rlnc_forwarding_sym32_relays010_e1_005_e2_0203_rs_23632_den0103_120220.json'

fulcr_name_orig_rlnc_recoder = path_to_multihops+'transmission/' + 'fulcrum_rlnc2_recoder_sym32_r2_relays010_e1_005_e2_0203_rs_23632_140220.json'

rlnc_name_orig_rlnc_recoder = path_to_multihops+'transmission/' + 'orig_rlnc2_recoder_sym32_relays010_e1_005_e2_0203_rs_23632_140220.json'

#new test for Frank

inputfile = path_to_multihops+'transmission/' + 'test_r2_s32_ad.json'
input_expansions = path_to_multihops+'transmission/' + 'rlnc_fulc_s16_r1_to_7_e1_005_e2_01_run10000.json'
new_input_expansions = path_to_multihops+'transmission/' + 'new_rlnc_fulc_s16_r1_to_7_e1_005_e2_01_run10000.json'
new_r8_input_expansions = path_to_multihops+'transmission/' + 'new_rlnc8_s16_r1_to_7_e1_005_e2_01_run10000.json'


input_erasure2s = path_to_multihops+'transmission/' + 'rlnc_fulc_s16_r47_e1_005_e2_005_to_03_run10000.json'
new_input_erasure2s = path_to_multihops+'transmission/' + 'new_rlnc_fulc_s16_r4_e1_005_e2_005_to_03_run5000.json'

input_fwd_expansion = path_to_multihops+'transmission/' + 'rlnc_fulc_forwarding_s1632_r1_to_7_e1_005_e2_01_run10000.json'
input_fwd_erasure2 = path_to_multihops+'transmission/' + 'rlnc_fulc_forwarding_s16_r4_e1_005_e2_005_to_03_run5000.json'

input_relays10 = path_to_multihops+'transmission/' + 'rlnc_fulc_s16_r24_e1_005_e2_01_relays_0_to_10_run10000.json'
new_input_relays10 = path_to_multihops+'transmission/' + 'rlnc_fulc_s16_r24_e1_005_e2_01_relays_0_to_10_run10000.json'
input_relays10_fwd = path_to_multihops+'transmission/' + 'new_rlnc_fulc_s16_r4_e1_005_e2_01_relays_0_to_10_run5000.json'

#e1 = 005, e2 = 03
input_expansions_e2_003 = path_to_multihops+'transmission/' + 'refwd_rlnc_fulc_s16_r1_to_7_e1_005_e2_03_run5000.json'
input_erasure2s_015_004 = path_to_multihops+'transmission/' + 'refwd_rlnc_fulc_s16_r4_e1_005_e2_015_to_04_run3000.json'
input_relays_015_03 = path_to_multihops+'transmission/' + 'refwd_rlnc_fulc_s16_r4_e1_005_e2_03_relays_0_to_10_run3000.json'





def ldp_vs_recoding_symbols():

    symbols = 32
    expansions = 2
    relay = 4

    density = 0.3

    recoding_symbols = [3, 6, 10, 12, 16, 20, 24]
    x_axis_values = np.arange(1, 8)

    measured = 'ldp'

    fulcr_inner = get_avg_ldp_recodingsymbols(fulcr_name_orig_recoder_full, relay, 'FulcrumInner', 2)
    fulcr_comb = get_avg_ldp_recodingsymbols(fulcr_name_orig_recoder_full, relay, 'FulcrumCombined', 2)

    dsep_inner = get_avg_ldp_recodingsymbols(dsep_name_new_recoder_full, relay, 'FulcrumInner', 2, 5)
    dsep_comb = get_avg_ldp_recodingsymbols(dsep_name_new_recoder_full, relay, 'FulcrumCombined', 2, 5)

    rlnc2 = get_avg_ldp_recodingsymbols(rlnc_name_orig_recoder_full, relay, 'RLNC2')
    rlnc8 = get_avg_ldp_recodingsymbols(rlnc_name_orig_recoder_full, relay, 'RLNC8')

    srlnc2 = get_avg_ldp_recodingsymbols(srlnc_name_orig_recoder_full, relay, 'RLNC2', 0, 0, density)
    srlnc8 = get_avg_ldp_recodingsymbols(srlnc_name_orig_recoder_full, relay, 'RLNC8', 0, 0, density)


    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    index = 0
    if measured == 'trans': index = 1

    print fulcr_inner
    plt.plot(x_axis_values, np.array(fulcr_inner[index])/np.float(symbols+expansions)*100, label='Orig. Fulc. Inner',
             color='c', marker=markers[0], fillstyle='none', linestyle='--')

    plt.plot(x_axis_values, np.array(fulcr_comb[index])/np.float(symbols)*100, label='Orig. Fulc. Comb.',
             color='c', marker=markers[0], fillstyle='none', linestyle='-')


    plt.plot(x_axis_values, np.array(dsep_inner[index])/np.float(symbols)*100, label='DSEP-R Fulc. Inner',
             color='r', marker=markers[5], linestyle='--', fillstyle='none')

    plt.plot(x_axis_values, np.array(dsep_comb[index])/np.float(symbols)*100, label='DSEP-R Fulc. Comb.',
             color='r', marker=markers[5], linestyle='-', fillstyle='none')

    plt.plot(x_axis_values, np.array(rlnc2[index])/np.float(symbols)*100, label='Full RLNC '+'$GF(2)$', linestyle='--', marker='^',
             fillstyle='none', color='g')

    plt.plot(x_axis_values, np.array(rlnc8[index])/np.float(symbols)*100, label='Full RLNC '+r'$GF(2^8)$', linestyle='-', marker='^',
             fillstyle='none', color='g')

    plt.plot(x_axis_values, np.array(srlnc2[index])/np.float(symbols)*100, label='Sparse RLNC '+'$GF(2)$', linestyle='--', color='blue', marker='>',
             fillstyle='none')

    plt.plot(x_axis_values, np.array(srlnc8[index])/np.float(symbols)*100, label='Sparse RLNC ' + '$GF(2^8)$', linestyle='-', color='blue',
             marker='>',
             fillstyle='none')

    plt.yscale('log')
    y_scale = [1, 2, 5, 10, 25, 40, 70]
    if relay == 4:
        y_scale = [2, 4, 8, 13, 20, 35, 100]
    # plt.yscale('log', linthreshy=10)
    # ax.yaxis.set_major_formatter(FormatStrFormatter('%g'))

    plt.yticks(list(scipy.unique(y_scale)), list(scipy.unique(y_scale)))


    plt.xlabel('Recoding Queue Size [symbols]')

    plt.ylabel('Overhead [\%]')
    if measured == 'trans':
        plt.ylabel('Number of Transmissions')
    # plt.legend(loc='best')

    plt.xticks(x_axis_values, recoding_symbols)
    plt.grid(True, lw=0.2)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.9])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=3,
               fancybox=True, shadow=False)

    # plt.legend(loc='best')

    output_filename = measured + '_with_DSEP_relay' + str(relay) +'_density_'+ str(density) + '.pdf'
    fig.set_size_inches(width, height)
    fig.savefig(output_path + 'trans/' + output_filename)

    plt.close()

def transmission_vs_relays():

    symbols = 16
    expansion = 4

    recoding_symbols = 0

    recoding = True

    e1 = 0.05
    e2 = 0.3

    relays = np.arange(0, 11)

    measured = 'trans'

    type_recoder = '_recoding'



    # get_avg_ldp_relays(filename, recoding_symbols, testcase='FulcrumCombined', expansion=0, redundant=0, density=0.0,
    #                    symbol=32, e1=0.1, e2=0.5, recoding=True):
    fulcr = get_avg_ldp_relays(input_relays_015_03, recoding_symbols, 'FulcrumCombined', expansion, 0, 0, symbols, e1, e2, recoding, relays)

    fulcr_ad = get_avg_ldp_relays(input_relays_015_03, recoding_symbols, 'FulcrumCombinedAD', expansion, 0, 0, symbols, e1, e2,
                               recoding, relays)

    rlnc2 = get_avg_ldp_relays(input_relays_015_03, recoding_symbols, 'RLNC2', 0, 0, 0, symbols, e1, e2, recoding, relays)

    rlnc8 = get_avg_ldp_relays(input_relays_015_03, recoding_symbols, 'RLNC8', 0, 0, 0, symbols, e1, e2, recoding, relays)

    rlnc2_fwd = get_avg_ldp_relays(input_relays_015_03, recoding_symbols, 'RLNC2', 0, 0, 0, symbols, e1, e2, False, relays)

    print 'fulcr:', fulcr
    print 'fulcrAD:', fulcr_ad

    print 'rlnc2:', rlnc2
    print 'rlnc8:', rlnc8

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    index = 0

    if measured == 'trans': index = 1

    # recoding

    plt.plot(relays, fulcr[index], label='Fulcrum - Original Recoder',
             color='blue', marker=markers[0], fillstyle='none', linestyle='-')

    plt.plot(relays, fulcr_ad[index], label='Fulcrum - New Recoder',
             color='green', marker=markers[1], fillstyle='none', linestyle='--')

    plt.plot(relays, rlnc2[index], label='RLNC ' + '$GF(2)$', linestyle='-', marker=markers[2],
             fillstyle='none', color='red')

    plt.plot(relays, rlnc8[index], label='RLNC ' + r'$GF(2^8)$', linestyle='-', marker=markers[3],
             fillstyle='none', color='black')
    #Forwarding

    plt.plot(relays, rlnc2_fwd[index], label='RLNC ' + r'$GF(2^8)$' + ' - Fwd', linestyle='dotted', marker=markers[4],
             fillstyle='none', color='brown')

    # forwarding
    #
    # plt.plot(relays, fw_fnc[index], label='Fulcrum - Fwd',
    #          color='blue', marker=markers[4], fillstyle='none', linestyle='dotted')
    #
    # plt.plot(relays, fw_rlnc2[index], label='RLNC ' + r'$GF(2)$' + ' - Fwd', linestyle='dotted', marker=markers[5],
    #          fillstyle='none', color='red')
    #
    # plt.plot(relays, fw_rlnc8[index], label='RLNC ' + r'$GF(2^8)$' + ' - Fwd', linestyle='dotted',
    #          marker=markers[6],
    #          fillstyle='none', color='black')

    plt.ylabel('Number of Linear Dependent Packets')
    yvalues = [0, 1.6, 5, 10, 15, 20, 25]
    if measured == 'trans':
        plt.ylabel('Number of Transmissions')
        yvalues = [20, 25, 30, 35, 40, 45]

    plt.yticks(yvalues)

    plt.xticks([0, 2, 4, 6, 8, 10])


    plt.grid(True, lw=0.2)

    plt.xlabel('Number of Recoders')



    fontsize = 6
    # plt.legend(loc='best', fontsize=fontsize)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.94])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=2,
               fancybox=True, shadow=False, fontsize=fontsize)

    output_filename = measured + '_vs_relays_s' + str(symbols) + '_e1_' + str(e1) + '_e2_' + str(e2)
    fig.set_size_inches(width, height)
    fig.savefig(path_destination + output_filename + '.pdf')
    fig.savefig(path_destination + output_filename + '.eps')

    plt.close()


def transmission_vs_expansions():

    symbols = 16
    relays = 1

    recoding_symbols = 0

    redundant = 5

    recoding = True

    e1 = 0.05
    e2 = 0.3

    density = 0.3

    measured = 'trans'

    type_recoder = '_recoding'
    expansions = np.arange(1, 8)


    # def get_avg_ldp_expansion(filename, recoding_symbols, testcase='FulcrumCombined', redundant=0, density=0.0,
    #                           symbol=32, e1=0.1, e2=0.5, recoding=True, relays=1):
    fulcr = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'FulcrumCombined',  0, 0, symbols, e1, e2, recoding, relays, expansions)

    fulcr_ad = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'FulcrumCombinedAD',  0, 0, symbols, e1, e2,
                               recoding, relays, expansions)

    rlnc2 = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'RLNC2', 0, 0, symbols, e1, e2, recoding, relays, expansions)

    rlnc8 = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'RLNC8', 0, 0, symbols, e1, e2, recoding, relays, expansions)

    fw_rlnc2 = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'RLNC2', 0, 0, symbols, e1, e2, False, relays, expansions)
    fw_rlnc8 = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'RLNC8', 0, 0, symbols, e1, e2, False,
                                     relays, expansions)

    fw_fnc = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'FulcrumCombined', 0, 0, symbols, e1, e2, False,
                                     relays, expansions)
    fw_fnc_outer = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'FulcrumOuter', 0, 0, symbols, e1, e2,
                                   False,
                                   relays, expansions)
    fw_fnc_ad = get_avg_ldp_expansion(input_expansions_e2_003, recoding_symbols, 'FulcrumCombinedAD', 0, 0, symbols, e1, e2,
                                         False,
                                         relays, expansions)

    fulcr_t = [25.4956, 25.2607, 25.07, 25.0312, 24.9022, 24.6145, 24.564]
    rlnc2_t = [26.7129, 26.7129, 26.7129, 26.7129, 26.7129, 26.7129, 26.7129]
    rlnc8_t = [23.0485, 23.0485, 23.0485, 23.0485, 23.0485, 23.0485, 23.0485]
    fw_rlnc2_t = [26.4692, 26.4692, 26.4692, 26.4692, 26.4692, 26.4692, 26.4692]

    print 'fulcr:', fulcr
    print 'fulcrAD:', fulcr_ad

    print 'rlnc2:', rlnc2
    print 'rlnc8:', rlnc8

    print 'fwd FNC:', fw_fnc
    print 'fwd FNC Outer:', fw_fnc_outer
    print 'fwd FNC AD:', fw_fnc_ad
    print 'fwd rln2:', fw_rlnc2
    print 'fwd rln8:', fw_rlnc8

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    index = 0

    print expansions
    if measured == 'trans': index = 1

    # recoding

    plt.plot(expansions, fulcr_t, label='Fulcrum - Original Recoder',
             color='blue', marker=markers[0], fillstyle='none', linestyle='-')

    plt.plot(expansions, fulcr_ad[index], label='Fulcrum - New Recoder',
             color='green', marker=markers[1], fillstyle='none', linestyle='--')

    plt.plot(expansions, rlnc2_t, label='RLNC '+'$GF(2)$', linestyle='-', marker=markers[2],
             fillstyle='none', color='red')

    plt.plot(expansions, rlnc8_t, label='RLNC '+r'$GF(2^8)$', linestyle='-', marker=markers[3],
             fillstyle='none', color='black')


    # forwarding

    # plt.plot(expansions, fw_fnc[index], label='Fulcrum - Fwd',
    #          color='blue', marker=markers[4], fillstyle='none', linestyle='dotted')

    plt.plot(expansions, fw_rlnc2_t, label='RLNC ' + r'$GF(2)$' + ' - Fwd', linestyle='dotted', marker=markers[5],
             fillstyle='none', color='brown')

    # plt.plot(expansions, fw_rlnc8[index], label='RLNC ' + r'$GF(2^8)$' + ' - Fwd', linestyle='dotted', marker=markers[6],
    #          fillstyle='none', color='black')

    plt.ylabel('Number of Linear Dependent Packets')
    yvalues = np.arange(0, 5)
    if measured == 'trans':
        plt.ylabel('Number of Transmissions')
        yvalues = np.arange(22, 29)

    plt.yticks(yvalues)

    plt.xticks(expansions)

    plt.grid(True, lw=0.2)

    plt.xlabel('Expansion Packets ' + r'$r$')



    fontsize = 6
    # plt.legend(loc='best', fontsize=fontsize)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.89])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=2,
               fancybox=True, shadow=False, fontsize=fontsize)

    output_filename = 'new_' + measured + '_vs_expansion_s' + str(symbols) + '_e1_'+ str(e1) + '_e2_'+ str(e2)
    fig.set_size_inches(width, height)
    fig.savefig(path_destination + output_filename + '.pdf')
    fig.savefig(path_destination + output_filename + '.eps')


    plt.close()

def transmission_vs_erasure2s():

    symbols = 16
    relays = 1

    recoding_symbols = 0

    redundant = 5

    recoding = True

    e1 = 0.05
    e2 = 0.3
    erasure2s = [0.15, 0.2, 0.25, 0.3, 0.35, 0.4]

    density = 0.3

    measured = 'trans'

    type_recoder = '_recoding'
    expansions = [4]

    results = {}
    results['FNC_trans'] = []
    results['FNC_AD_trans'] = []
    results['RLNC2_trans'] = []
    results['RLNC8_trans'] = []
    results['FNC_ld'] = []
    results['FNC_AD_ld'] = []
    results['RLNC2_ld'] = []
    results['RLNC8_ld'] = []

    results['FNC_trans_fwd'] = []
    results['RLNC2_trans_fwd'] = []
    results['RLNC8_trans_fwd'] = []
    results['FNC_ld_fwd'] = []
    results['RLNC2_ld_fwd'] = []
    results['RLNC8_ld_fwd'] = []
    # def get_avg_ldp_expansion(filename, recoding_symbols, testcase='FulcrumCombined', redundant=0, density=0.0,
    #                           symbol=32, e1=0.1, e2=0.5, recoding=True, relays=1, expansions):
    for e2 in erasure2s:

        #recoding calculation
        fulcr = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'FulcrumCombined',  0, 0, symbols, e1, e2, recoding, relays, expansions)

        fulcr_ad = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'FulcrumCombinedAD',  0, 0, symbols, e1, e2,
                               recoding, relays, expansions)

        rlnc2 = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'RLNC2', 0, 0, symbols, e1, e2, recoding, relays, expansions)

        rlnc8 = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'RLNC8', 0, 0, symbols, e1, e2, recoding, relays, expansions)

        results['FNC_ld'].append(fulcr[0][0])
        results['FNC_trans'].append(fulcr[1][0])
        results['FNC_AD_ld'].append(fulcr_ad[0][0])
        results['FNC_AD_trans'].append(fulcr_ad[1][0])
        results['RLNC2_ld'].append(rlnc2[0][0])
        results['RLNC2_trans'].append(rlnc2[1][0])
        results['RLNC8_ld'].append(rlnc8[0][0])
        results['RLNC8_trans'].append(rlnc8[1][0])

        # Forwarding calculation
        fulcr_fwd = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'FulcrumCombined', 0, 0, symbols, e1, e2,
                                      False, relays, expansions)

        rlnc2_fwd = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'RLNC2', 0, 0, symbols, e1, e2, False,
                                      relays, expansions)

        rlnc8_fwd = get_avg_ldp_expansion(input_erasure2s_015_004, recoding_symbols, 'RLNC8', 0, 0, symbols, e1, e2, False,
                                      relays, expansions)


        results['FNC_ld_fwd'].append(fulcr_fwd[0][0])
        results['FNC_trans_fwd'].append(fulcr_fwd[1][0])
        results['RLNC2_ld_fwd'].append(rlnc2_fwd[0][0])
        results['RLNC2_trans_fwd'].append(rlnc2_fwd[1][0])
        results['RLNC8_ld_fwd'].append(rlnc8_fwd[0][0])
        results['RLNC8_trans_fwd'].append(rlnc8_fwd[1][0])
    print results

    forwarding = '_fwd'
    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    index = 0

    #Recoding
    plt.plot(erasure2s, results['FNC_'+measured], label='Fulcrum - Original Recoder',
             color='blue', marker=markers[0], fillstyle='none', linestyle='-')

    plt.plot(erasure2s, results['FNC_AD_'+measured], label='Fulcrum - New Recoder',
             color='green', marker=markers[1], fillstyle='none', linestyle='--')

    plt.plot(erasure2s, results['RLNC2_'+measured], label='RLNC '+'$GF(2)$', linestyle='-', marker=markers[2],
             fillstyle='none', color='red')

    plt.plot(erasure2s, results['RLNC8_'+measured], label='RLNC '+r'$GF(2^8)$', linestyle='-', marker=markers[3],
             fillstyle='none', color='black')

    # Forwarding
    # plt.plot(erasure2s, results['FNC_' + measured + forwarding], label='Fulcrum - Fwd',
    #          color='blue', marker=markers[4], fillstyle='none', linestyle='dotted')

    plt.plot(erasure2s, results['RLNC2_' + measured + forwarding], label='RLNC ' + r'$GF(2)$' + ' - Fwd', linestyle='dotted', marker=markers[5],
             fillstyle='none', color='brown')

    # plt.plot(erasure2s, results['RLNC8_' + measured + forwarding], label='RLNC ' + r'$GF(2^8)$' + ' - fwd', linestyle='dotted', marker=markers[6],
    #          fillstyle='none', color='black')

    plt.xticks(erasure2s)

    plt.grid(True, lw=0.2)

    plt.xlabel('Erasure Probability of ' + r'$\epsilon_2$')

    plt.ylabel('Number of Linear Dependent Packets')
    if measured == 'trans':
        plt.ylabel('Number of Transmissions')
        plt.yticks([18, 20, 22, 24, 26, 28, 30, 32])

    fontsize = 6
    # plt.legend(loc='best', fontsize=fontsize)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.89])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=2,
               fancybox=True, shadow=False, fontsize=fontsize)

    output_filename = measured + '_vs_erasure2s_s' + str(symbols) + '_e1_'+ str(e1) + '_e2_'+ str(e2)
    fig.set_size_inches(width, height)
    fig.savefig(path_destination + output_filename + '.pdf')
    fig.savefig(path_destination + output_filename + '.eps')


    plt.close()


def get_avg_throughput_vs_relays(filename, measured_type='en_throughput', testcase='FulcrumCombined', expansion=0, redundant=0, density=0.0, symbol=32, e1=0.1, e2=0.5, recoding_symbols=3, relays=[0, 2, 4, 6, 8, 10]):

    df = open_file(filename)

    df = df[(df['testcase'] == testcase) & (df['symbols'] == symbol) &
            (df['erasure1'].round(2) == np.around(e1, 2)) & (df['erasure2'].round(2) == np.around(e2, 2))]

    if expansion > 0:
        df = df[(df['expansions'] == expansion)]

    if redundant > 0:
        df = df[(df['redundant'] == redundant)]

    if density > 0.0:
        df = df[(df['density'].round(2) == np.around(density, 2))]

    if recoding_symbols >= 0:
        df = df[df['recoding_symbols'] == recoding_symbols]

    # print df
    # relays = [0, 2, 4, 6, 8, 10]
    # relays = [2, 6, 10]


    throughput = []

    for relay in relays:
        df1 = df[df['num_relays'] == relay]
        df2 = df1[measured_type]
        for v in df2:
            throughput.append(np.mean(v))

    return throughput

def throughput_vs_relays_bar():

    bar_width = 0.10
    opacity = 0.7
    N = 3
    ind = np.arange(N)

    symbols = 32
    expansion = 2
    recoding_symbols = 3

    redundant = 5

    density = 0.3

    e1 = 0.05
    e2 = 0.3

    relays = [2, 6, 10]

    measured_type = 're_throughput'

    fulinner = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumInner', 2, 0, 0, symbols, e1, e2,
                                       symbols)
    # fulcombined = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumCombined', 2, 0, 0, symbols, e1, e2, symbols)

    # fulouter = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumOuter', 2, 0, 0, 32, e1, e2,
    #                                    32)

    # dsepinner = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumInner', 2, redundant, 0, 32, e1, e2,
    #                                    recoding_symbols)
    dsepcombined_rc3 = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumCombined', 2, redundant, 0, 32, e1, e2,
                                       recoding_symbols)
    dsepcombined_rc6 = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumCombined', 2, redundant, 0, 32, e1,
                                                e2, 6)
    # dsepouter = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumOuter', 2, redundant, 0, 32, e1, e2,
    #                                    recoding_symbols)

    rlnc2 = get_avg_throughput_vs_relays(rlnc_name, measured_type, 'RLNC2', 0, 0, 0, symbols, e1, e2,
                                       symbols)

    rlnc8 = get_avg_throughput_vs_relays(rlnc_name, measured_type, 'RLNC8', 0, 0, 0, symbols, e1, e2,
                                       symbols)

    # srlnc2 = get_avg_throughput_vs_relays(srlnc_name, measured_type, 'RLNC2', 0, 0, 0.3, 32, e1, e2,
    #                                      32)
    #
    # srlnc8 = get_avg_throughput_vs_relays(srlnc_name, measured_type, 'RLNC8', 0, 0, 0.3, 32, e1, e2,
    #                                      32)


    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    ax.bar(ind, dsepcombined_rc3, bar_width, alpha=opacity,
           label='Orig. Fulc., rec., ' +r'$GF(2)$, ' + str(recoding_symbols), lw=0, color='r')

    ax.bar(ind + 1 * bar_width, dsepcombined_rc6, bar_width, alpha=opacity,
           label='Orig. Fulc., rec., ' +r'$GF(2)$, ' + str(6), lw=0, color='orange')

    print fulinner

    ax.bar(ind + 2 * bar_width, fulinner, bar_width, alpha=opacity,
           label='Orig. Fulc., rec., ' + r'$GF(2)$, ' + str(symbols),
           lw=0, color='b')
    # ax.bar(ind + 1 * bar_width, fulouter, bar_width, alpha=opacity, label='Fulcr. Outer',
    #        lw=0, color='blue')

    ax.bar(ind + 3 * bar_width, rlnc2, bar_width, alpha=opacity,
           label='Full RLNC, rec., ' +r'$GF(2)$, ' + str(symbols), lw=0, color='green')

    ax.bar(ind + 4 * bar_width, rlnc8, bar_width, alpha=opacity,
           label='Full RLNC, rec., ' + r'$GF(2^8)$, ' + str(symbols), lw=0, color='brown')

    # ax.bar(ind + 6 * bar_width, srlnc2, bar_width, alpha=opacity,
    #        label='Sparse RLNC ' + r'$GF(2)$', lw=0, color='orange')
    # ax.bar(ind + 7 * bar_width, srlnc8, bar_width, alpha=opacity,
    #        label='Sparse RLNC ' + r'$GF(2^8)$', lw=0, color='orange')
    # if testcase == 'Inner':
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaInner'], bar_width, alpha=opacity,
    #            label='SISO Inner', color=colors[0], lw=0, hatch='/////')
    # elif testcase == 'Outer':
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaOuter'], bar_width, alpha=opacity,
    #            label='SISO Outer', lw=0, color=colors[1], hatch='.....', )
    # else:
    #     ax.bar(ind + 3 * bar_width, spaspa.iloc[1:9]['FulcrumSpaspaCombined'], bar_width, alpha=opacity,
    #            label='SISO Combined', lw=0, color=colors[2], hatch='xxxxx')

    plt.xlabel('Number of Relay Nodes')
    plt.ylabel('Recoding Throughput [Mbyte/s]')


    ax.set_xticks(ind + 2 * bar_width)

    ax.set_xticklabels(relays)
    ax.tick_params(width=0)

    yticks = [200, 500, 1000, 1500, 2000, 2300]
    plt.ylim(200, 2300)
    plt.yticks(yticks)
    # Hoac: plt.xticks(index + bar_width / 2, ('A', 'B', 'C', 'D', 'E'))

    # plt.legend(loc='best')
    fontsize = 6.4
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.9])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=2,
               fancybox=True, shadow=False, fontsize=fontsize)
    plt.grid(True)
    fig.set_size_inches(width, height)
    filename = output_path + 'throughput/' + measured_type + 'bar_rc_' + str(recoding_symbols) + '_red_'+str(redundant) +'_den_' + str(density) + '_e1_' + str(e1) + '_e2_' + str(e2)+ '.pdf'
    fig.savefig(filename)
    plt.close()

def goodput_vs_relays_line():

    recoding_symbols = 3

    redundant = 20

    density = 0.3

    symbols = 32

    e1 = 0.05
    e2 = 0.3

    relays = [0, 2, 4, 6, 8, 10]

    measured_type = 're_goodput' #if re_throughput, you have to change the
    index_start = 0

    if (measured_type[0:2] == 're'):
        index_start = 1
        relays = [2, 4, 6, 8, 10]


    fulinner = get_avg_throughput_vs_relays(fulcr_name_orig_recoder_rs610, measured_type, 'FulcrumInner', 2, 0, 0, 32, e1, e2,
                                            10, relays)

    fulcombined = get_avg_throughput_vs_relays(fulcr_name_orig_recoder_rs610, measured_type, 'FulcrumCombined', 2, 0, 0, 32, e1, e2,
                                               10, relays)

    #if using dsep_name_inner, we have full relays: 0 1 2 4 6 8 10
    dsepinner = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumInner', 2, redundant, 0, 32, e1, e2,
                                             recoding_symbols, relays ) #, [0, 1, 2, 4, 6, 8, 10] if dsep_name_inner

    dsepcombined = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumCombined', 2, redundant, 0, 32,
                                                e1, e2, recoding_symbols, relays)


    rlnc2 = get_avg_throughput_vs_relays(rlnc_name_orig_recoder_rs610, measured_type, 'RLNC2', 0, 0, 0, 32, e1, e2,
                                         10, relays)

    rlnc8 = get_avg_throughput_vs_relays(rlnc_name_orig_recoder_rs610, measured_type, 'RLNC8', 0, 0, 0, 32, e1, e2,
                                         10, relays)

    srlnc2 = get_avg_throughput_vs_relays(srlnc_name_orig_recoder_rs610, measured_type, 'RLNC2', 0, 0, density, 32, e1, e2,
                                          10, relays)

    srlnc8 = get_avg_throughput_vs_relays(srlnc_name_orig_recoder_rs610, measured_type, 'RLNC8', 0, 0, density, 32, e1, e2,
                                          10, relays)
    # print srlnc2

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    print dsepinner
    print dsepcombined
    # print rlnc8
    print fulinner
    print fulcombined

    plt.plot(relays, dsepinner, label='DSEP-R Fulc. Inner',
             color='r', marker=markers[5], linestyle='--', fillstyle='none')

    plt.plot(relays, dsepcombined, label='DSEP-R Fulc. Combined',
             color='r', marker=markers[5], linestyle='-', fillstyle='none')

    index_start = 0 #because we measure from 2 intermediate node for orig recoder

    plt.plot(relays, fulinner, label='Orig. Fulc. Inner',
             color='c', marker=markers[0], fillstyle='none', linestyle='--')

    plt.plot(relays, fulcombined, label='Orig. Fulc. Combined',
             color='c', marker=markers[0], fillstyle='none', linestyle='-')




    plt.plot(relays, rlnc2, label='Full RLNC, ' + '$GF(2)$', linestyle='--', marker='^',
             fillstyle='none', color='g')

    plt.plot(relays, rlnc8, label='Full RLNC, ' + r'$GF(2^8)$', linestyle='-', marker='^',
             fillstyle='none', color='g')

    plt.plot(relays, srlnc2, label='Sparse RLNC, ' + '$GF(2)$', linestyle='--', color='blue', marker='<',
             fillstyle='none')

    plt.plot(relays, srlnc8, label='Sparse RLNC, ' + '$GF(2^8)$', linestyle='-', color='blue',
             marker='<',
             fillstyle='none')


    # y_scale = [45, 50, 60, 70, 80]
    #
    # if (e2 == 0.2):
    #     y_scale = [38, 45, 50, 65, 72]
    #
    # plt.ylim((np.min(y_scale), np.max(y_scale)))

    # plt.yscale('log', linthreshy=10)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%g'))

    # plt.yticks(list(scipy.unique(y_scale)), list(scipy.unique(y_scale)))
    # plt.yticks([40, 50, 60, 70])

    y_ticks = [200, 400, 600, 800, 1000, 1200]
    if redundant == 20:
        y_ticks = [200, 400, 600, 800, 1000, 1200, 1400]

    # if density == 0.1:
    #     y_ticks = [200, 400, 600, 800, 1000, 2100]


    plt.xticks(relays[index_start:])

    plt.grid(True, lw=0.2)

    plt.xlabel('Number of Intermediate Nodes')

    plt.ylabel('Decoding Goodput [MB/s]')
    if (measured_type[0:2] == 're'):
        plt.ylabel('Recoding Goodput [MB/s]')
        y_ticks = [30, 70, 150, 250, 350, 450]
        if recoding_symbols == 6:
            y_ticks = [50, 150, 250, 350, 450, 550, 650]
        if recoding_symbols in [2]:
            y_ticks = [20, 200, 400, 600, 800, 1000]
        if recoding_symbols in [3]:
            y_ticks = [20, 200, 400, 600, 800]



    plt.yticks(y_ticks)

    plt.ylim(np.min(y_ticks), np.max(y_ticks))

    # plt.legend(loc='best')
    fontsize = 4.9
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.9])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=3,
               fancybox=True, shadow=False, fontsize=fontsize)


    plt.grid(True)
    fig.set_size_inches(width, height)
    filename = output_path + 'goodput/' + measured_type + '_rc_' + str(recoding_symbols) + '_red_' + str(
        redundant) + '_den_' + str(density) + '_e1_' + str(e1) + '_e2_' + str(e2) + '.pdf'
    fig.savefig(filename)
    plt.close()

def recoding_throughput_vs_relays_line():

    symbols = 32
    recoding_symbols = 3

    expansion = 2

    redundant = 5

    density = 0.3

    e1 = 0.05
    e2 = 0.3

    relays = [0, 2, 4, 6, 8, 10]

    measured_type = 're_throughput' #if re_throughput, you have to change the
    index_start = 0

    if (measured_type[0:2] == 're'):
        index_start = 1


    fulinner_rc3 = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumInner', expansion, 0, 0, symbols, e1, e2,
                                            recoding_symbols, relays)

    fulcombined_rc3 = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumCombined', expansion, 0, 0, symbols, e1, e2,
                                               recoding_symbols, relays)

    fulinner = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumInner', expansion, 0, 0, symbols, e1,
                                                e2, symbols, relays)

    fulcombined = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumCombined', expansion, 0, 0,
                                                   symbols, e1, e2, symbols, relays)

    fulcombined_rc6 = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumCombined', expansion, 0, 0,
                                                   symbols, e1, e2, 6, relays)


    fulouter = get_avg_throughput_vs_relays(fulcr_name, measured_type, 'FulcrumOuter', expansion, 0, 0, symbols, e1, e2,
                                            symbols, relays)

    dsepinner = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumInner', expansion, redundant, 0, symbols, e1, e2,
                                             recoding_symbols, relays)

    dsepcombined = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumCombined', expansion, redundant, 0, symbols,
                                                e1, e2, recoding_symbols, relays)

    dsepcombined_rc6 = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumCombined', expansion, redundant, 0, symbols,
                                                e1, e2, 6, relays)

    dsepouter = get_avg_throughput_vs_relays(dsep_name, measured_type, 'FulcrumOuter', expansion, redundant, 0, symbols, e1, e2,
                                             recoding_symbols, relays)

    rlnc2 = get_avg_throughput_vs_relays(rlnc_name, measured_type, 'RLNC2', 0, 0, 0, symbols, e1, e2,
                                         symbols, relays)

    rlnc8 = get_avg_throughput_vs_relays(rlnc_name, measured_type, 'RLNC8', 0, 0, 0, symbols, e1, e2,
                                         symbols, relays)

    srlnc2 = get_avg_throughput_vs_relays(srlnc_name, measured_type, 'RLNC2', 0, 0, 0.3, symbols, e1, e2,
                                          symbols, relays)

    srlnc8 = get_avg_throughput_vs_relays(srlnc_name, measured_type, 'RLNC8', 0, 0, 0.3, symbols, e1, e2,
                                          symbols, relays)

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.16, bottom=.19, right=0.92, top=.92)

    # plt.plot(relays[index_start:], dsepinner[index_start:], label='DSEP-R Fulc. Inner',
    #          color='r', marker=markers[5], linestyle='--', fillstyle='none')

    #present the limited queue size

    print dsepinner
    plt.plot(relays[index_start:], dsepinner[index_start:],
             label='Orig. Fulc., rec., GF(2), ' + str(recoding_symbols) +' with DSEP Inner',
             color='r', marker=markers[5], linestyle='--', fillstyle='none')

    plt.plot(relays[index_start:], dsepcombined[index_start:], label='Orig. Fulc., rec., GF(2), ' + str(recoding_symbols) + ' with DSEP Comb.',
             color='r', marker=markers[5], linestyle='-', fillstyle='none')



    # plt.plot(relays[index_start:], dsepcombined_rc6[index_start:],
    #          label='Orig. Fulc, rec., GF(2), ' + str(6),
    #          color='r', marker=markers[5], linestyle='-', fillstyle='none')


    # plt.plot(relays[index_start:], fulinner[index_start:], label='Orig. Fulc. Inner',
    #          color='c', marker=markers[0], fillstyle='none', linestyle='--')

    #present the unlimited queue size

    plt.plot(relays[index_start:], fulinner_rc3[index_start:],
             label='Orig. Fulc., rec., GF(2), ' + str(recoding_symbols) +' with Fulc. Inner',
             color='b', marker=markers[0], fillstyle='none', linestyle='-')

    plt.plot(relays[index_start:], fulcombined_rc3[index_start:],
             label='Orig. Fulc., rec., GF(2), ' + str(recoding_symbols) + ' with Fulc. Comb.',
             color='b', marker=markers[0], fillstyle='none', linestyle='--')


    plt.plot(relays[index_start:], fulinner[index_start:], label='Orig. Fulc., rec., GF(2), ' + str(symbols) + 'with Fulc. Inner',
             color='c', marker=markers[0], fillstyle='none', linestyle='--')

    plt.plot(relays[index_start:], fulcombined[index_start:],
             label='Orig. Fulc., rec., GF(2), ' + str(symbols) + ' with Fulc. Comb.',
             color='c', marker=markers[0], fillstyle='none', linestyle='-')

    # color, marker = getCM('DSEP-R Fulc. ')



    plt.plot(relays[index_start:], rlnc2[index_start:], label='Full RLNC, rec./dec., ' + '$GF(2)$, '  + str(symbols), linestyle='--', marker='^',
             fillstyle='none', color='g')

    plt.plot(relays[index_start:], rlnc8[index_start:], label='Full RLNC, rec./dec., ' + r'$GF(2^8)$, ' + str(symbols), linestyle='-', marker='^',
             fillstyle='none', color='g')
    print rlnc8

    # plt.plot(relays[index_start:], srlnc2[index_start:], label='Sparse RLNC, ' + '$GF(2)$', linestyle='--', color='blue', marker='<',
    #          fillstyle='none')
    #
    # plt.plot(relays[index_start:], srlnc8[index_start:], label='Sparse RLNC, ' + '$GF(2^8)$', linestyle='-', color='blue',
    #          marker='<',
    #          fillstyle='none')

    # plt.plot(relays, rlnc2_fw[index], label='Full RLNC ' + '$GF(2)$' + ' forwarding', linestyle='--', marker='>',
    #          fillstyle='none', color='k')
    #
    # plt.plot(relays, rlnc8_fw[index], label='Full RLNC ' + r'$GF(2^8)$' + ' forwarding', linestyle='-', marker='>',
    #          fillstyle='none', color='k')


    # y_scale = [45, 50, 60, 70, 80]
    #
    # if (e2 == 0.2):
    #     y_scale = [38, 45, 50, 65, 72]
    #
    # plt.ylim((np.min(y_scale), np.max(y_scale)))

    # plt.yscale('log', linthreshy=10)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%g'))

    # plt.yticks(list(scipy.unique(y_scale)), list(scipy.unique(y_scale)))
    # plt.yticks([40, 50, 60, 70])


    plt.xticks(relays[index_start:])

    plt.grid(True, lw=0.2)

    plt.xlabel('Number of Relay Nodes')

    plt.ylabel('Recoding Throuhgput [MB/s]')

    yticks = [200, 500, 1000, 1500, 2000, 2300]
    plt.yticks(yticks)
    plt.ylim(np.min(yticks), np.max(yticks))

    # plt.legend(loc='best')

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 - box.height * 0.03, box.width * 1.05, box.height * 0.9])
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=2,
               fancybox=True, shadow=False)


    plt.grid(True)
    fig.set_size_inches(width, height)
    filename = output_path + 'throughput/' + measured_type + '_diff_decoders_rc_' + str(recoding_symbols) + '_red_' + str(
        redundant) + '_den_' + str(density) + '_e1_' + str(e1) + '_e2_' + str(e2) + '.pdf'
    fig.savefig(filename)
    plt.close()


if __name__ == '__main__':


    # trans_recoding_vs_forwarding()
    ##DSEP paper
    # throughput_vs_relays_bar()
    # recoding_throughput_vs_relays_line()

    ##Globecom paper
    # ldp_vs_recoding_symbols()
    # goodput_vs_relays_line()

    #new test for Frank
    #transmission_vs_relays()
    transmission_vs_expansions()
    # transmission_vs_erasure2s()
