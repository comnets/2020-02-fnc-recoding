\section{Introduction}
\label{sec:intro}

Future communication systems will be massively heterogeneous in terms of
computing capability and memory. The system should ensure the harmonious coexistence of a wide spectrum of devices including Internet of
Things (IoT) with very limited computing power together with
heterogeneous smartphones and tablets, and other network nodes such as
driverless vehicles as well as robots. To improve
the performance of such systems, it is important to consider their heterogeneity
when designing codes. Fulcrum codes are a type of random network codes that
allow heterogeneous devices to participate in the same coding
strategy~\cite{Lucani2018}. A sender encodes the information and add redundancy
with an outer and an inner code using finite fields of different sizes, i.e., a
large finite field with computationally expensive operations, and a small finite
field with cheap operations~\cite{7565066}. The coded information is usually
assumed to be transmitted over erasure channels. The destinations can decode the
encoded information by using one of the two finite fields employed at the
encoder. The idea is that a destination who is computationally limited (e.g.,
due to processing limitations or energy saving requirements), can wait for a few
extra redundant packets and decode using the smaller field size and cheap
operations. At the same time, another destination that is computationally
capable can spend more computing power and decode using the bigger finite field,
and less coded packets. Therefore, there is a trade-off between the number of
received packets and computational complexity for decoding. Interestingly, as a network
code, Fulcrum codes allow intermediate nodes to participate in the coding
process by means of recoding. The recoding is computationally cheap since the
recoder nodes operate with the small field size. This is beneficial since
intermediate nodes are usually the routers that have to deal with potentially
large number of streams, or other heterogeneous nodes in meshed networks.

Over the past years, research in Fulcrum codes, such as dynamic sparsity and expansion packets (DSEP) 
Fulcrum~\cite{nguyen2020dsep}, focused on reducing the complexity of the encoders and
decoders.
% by adding sparsity to the coding coefficients of the linear combinations, or
% designing smarter decoders that would choose the correct field for decoding
% based on the number of received packets.
However, there has been a limited improvement in the recoders. They perform
cheap operations, but they might be memory-limited, and depending on the number
of streams or the sizes of the blocks of data used at the encoders
(generations), the recoders might not perform correctly. Current implementations
assume the recoders will store all the received coded packets of
a generation in its buffers to recombine them until the decoders can decode. However, if the
generations or the number of data streams the node are recoding are large, the
memory requirements might exceed the buffers sizes. Furthermore, if the encoders
use smart sparsity techniques such as DSEP Fulcrum encoders, then the recoding
process tends to destroy the sparsity characteristics of the transmissions. 

In
this paper we show that we can further reduce the resources needed at the
recoder nodes by reducing the memory employed for recoding. We show that the
performance of Fulcrum codes do not degrade if the recoder nodes do not store
all the packets of a generation, but instead only a few packets. For example, we
show that storing one fifth of the generation in each intermediate node, can
reduce the linear dependencies of the system as much as storing the full
generation. This can drastically reduce the overhead of networks with multiple
hops. Furthermore, we show a technique for choosing the packets to store aimed
to preserve the sparsity of the recoded packets.
\begin{figure*}[!t]
	\centering
	\includegraphics[width=6.2in,height=2.3in]{figures/concept-smallsize.pdf}
	\caption{The original packets are encoded, recoded and decoded using Fulcrum
		codes in a multihop networks with different losses on the
		links. The intermediate nodes $ \mathbf{R_1} $ and $ \mathbf{R_2} $ are using different buffer
		sizes $\mathbf{B_1 }$ and $ \mathbf{B_2} $ to store the coded packets.}
	\label{fig:concept}
	\vspace{-1.5em}
\end{figure*}
 The remainder of this paper is organized as follows: In Section~II, we review the related work. 
 Section~III describes the operation of state of the art Fulcrum codes and DSEP
 Fulcrum. Section~IV introduces the recoding mechanism with recoders limited in
 memory, and Section~V presents the numerical results when using
 our proposed scheme.
