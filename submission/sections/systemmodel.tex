\section{Fulcrum and DSEP Fulcrum codes}
\label{section:encodingdecoding}
This section present the original Fulcrum codes and DSEP Fulcrum codes variation.
\subsection{Fulcrum Codes}
We consider the case of a sender transmitting a batch of $ n $ data packets,
typically called a generation e.g., a picture as shown in
Fig.~\ref{fig:concept}, to the receivers. 
%Refer to Table~\ref{table_notations} for the notation used throughout the paper. 
In the figure, $ n $ original packets
$ {p_1, p_2,..., p_n} $ are expanded into $ n+r $ packets, whereby the $ r $
expansion packets are generated as follows
\begin{equation}
o_k = \sum\limits_{j=1}^{n} c_{k,j} p_j,~ k = 1, 2,..., r,
\label{eq:outer_encoding}
\end{equation}
where $ c_{k,j} $ are the outer coding vectors, randomly selected by the encoder
from a large finite field $ GF(2^h) $, e.g., with $ h = 8 $. This stage is also called \textit{pre-coding} and generally
performed by a \textit{systematic code}. These $ r $ outer-coded packets $ o_i $
along with $ n $ original ones $ p_j $ later are treated as the new packets $ O
= \{p_1, p_2,...p_n\} \cup \{o_1, o_2,...,o_r\}$ that will be encoded in $ GF(2)
$ to generate inner-coded packets $ i_k $ as follows
\begin{equation}
i_k = \sum\limits_{j=1}^{n+r} \lambda_{k,j} O_j,~ k=1, 2, ..., n,...,
\label{eq:inner_encoding}
\end{equation}
where $ \lambda_{k,j} $ are the inner coding vectors, randomly selected by the encoder from $ GF(2) $.
These inner-coded packets $ i_k $, which are encoded by either a \textit{systematic code} or a \textit{non-systematic code} and then sent throughout the network, are easy to be recoded at the intermediate nodes using $ GF(2) $ operations. This provides simple recoding operations at the routers or devices in the network.

A receiver can use different types of decoders to operate on either the inner or
outer code, depending on its computing capabilities. The receiver employing a
inner decoder can take advantage of low computation complexity thanks to
decoding in $ GF(2) $. But it has to wait longer because it requires $ n+r $
linear independent packets. Whereas, outer and combined decoders require only $
n $ linear independent packets to recover the original packets with high
decoding probability, but they must partially decode in $GF(2^h)$ \cite{Lucani2018,Nguyen2018}.

%However, the receiver deploying outer code has to perform the particular steps to convert the inner to outer code before carrying out the decoding process in high field $ GF(2^h) $. This leads to a huge increase the number of unnecessary operations, happening in the extra coefficient vectors, which are set up at the beginning of the coding session. That motivates us to propose the tunable expansion packets, which reduces the complexity, increases the processing speed, while maintaining high decoding probability and low overhead.
\subsection{DSEP Fulcrum codes}

In order to mitigate the computational complexity during the coding process
while maintaining the high decoding probability of Fulcrum codes, we proposed
the DSEP Fulcrum codes in \cite{nguyen2020dsep} that is the combination of dynamic
sparse coding in Fulcrum and dynamic tuning the number of outer coding expansion
packets. The DSEP Fulcrum codes cope with the challenges of static sparsity $ w $ in
RLNC code and fixed expansion packets $ r $ in Fulcrum codes by dynamically
adjusting these parameters in the encoding process. i) To make the dynamic
sparse inner coding coefficients, we define $ w(k) $ \cite[Eqn.~(13)]{nguyen2020dsep}
(where $ k = 1, 2, ..., n + r$) as the number of packets from the set $ O =
\{p_1, p_2,...,p_n, o_{n+1},...,o_{n+r}\}$ of original data packets and
outer-coded packets  that are to be combined for forming the next inner-coded
packet $ i_k $ in Eqn.~(\ref{eq:inner_encoding}). The function $ w(k) $
automatically adjust the sparsity level toward each new inner-coded packet whose
value is set from $ 1 $ and not exceed $ (n + r)/2 $. The low sparsity level of
inner-coded packets transmitted benefit the computational complexity at the
beginning of a generation, while the increase of the sparsity levels at the end
of a generation helps to reduce the number of linearly dependent packets
\cite{nguyen2020dsep,Feizi2014}. This leads to an increase of the decoding probability
over \mbox{S-RLNC}, and reduces the computation complexity compared to the
conventional Fulcrum codes. ii) To adjust the expansion packets,
%the encoder firstly generates the inner-coded packets without the expansion packets at the beginning of a generation and then to increase the number of expansion packets included in the formation of inner-coded packets towards the end of a generation. When nearing the end of a generation, the decoder has collected a large number of linearly independent coded packets, which decreases the probability of newly received coded packets being linearly independent.
%More specifically, instead of including all $n$ original data packets and all $r$ expansion packets in the formation of inner
%coded packets $ I_\ell = \sum\limits_{j=1}^{n+r} \lambda_{\ell, j} o_j $, 
we define a new variable $ \mu,\ 0
\leq \mu \leq r$, as the number of expansion packets that are
considered for the inner encoding process. Thus, the inner-coded packets generated in Eqn.~(\ref{eq:inner_encoding}) can be rewritten as follows  

\begin{equation}
 i_k = \sum\limits_{j=1}^{n+\mu} \lambda_{k,j}  O_j,~ k=1, 2, ..., n,...,
\label{eq:inner_encoding_mu}
\end{equation}
where the number of non-zero elements of  $ \lambda_{k,j}  $ depends on the function $ w(k) $. 
With both adjustments, the DSEP Fulcrum encoder can create the inner-coded packets with strong robustness and a high decoding probability against packet losses over error-prone networks, while reducing the computation complexity at encoder, recoder and decoder.

\section{Recoding model}
\label{section:systemmodel}
This section introduces two different mechanisms of recoding with unlimited buffer and limited buffer size.
\begin{figure}[!t]
	\centering
	\includegraphics[width=3.3in]{figures/recoding.eps}
	\caption{An illustration of recoding process: inner-coded packets are sent through lost channels, an intermediate node is receiving and buffering the coded packets with a limited memory.
	}
	\label{fig:recoding}
	\vspace{-1.5em}
\end{figure}
\subsection{An unlimited buffer recoding}
\label{subsec:unlim_buf}
One of the main goals of Fulcrum codes is to make recoding operations
at the routers or intermediate nodes as simple as possible without compromising
network coding capabilities. Essentially, the intermediate nodes receive the
inner-coded packets in $ GF(2) $, store them into the buffers and recreate the
inner-recoded packets to the next hops as follows
%\begin{equation}
%o_i = \sum\limits_{j=1}^{n} c_{i,j} p_j,~ i = 1, 2,..., r,
%\label{eq:outer_encoding}
%\end{equation}
%where $ c_{i,j} $ are the outer coding vectors, randomly selected by the encoder from $ GF(2^h) $. These $ r $ outer coded packets $ o_i $ along with $ n $ original ones $ p_j $ later are treated as the new packets $ O = \{p_1, p_2,...p_n\} \cup \{o_1, o_2,...,o_r\}$ that will be encoded in $ GF(2) $, called inner code $ I_i $, as follows.
\begin{equation}
i'_k = \sum\limits_{j=1}^{n+r} \gamma_{k,j} i_j,~ k=1, 2, ..., n,...,
\label{eq:inner_recoding}
\end{equation}
where $ \gamma_{k,j} $ are the inner recoding vectors, randomly selected by an
intermediate node from $ GF(2) $. For this case, the intermediate nodes buffer
all received inner-coded packets for a generation. 
%In particular, more coded packets are buffered, more combinations are created
%at the intermediate nodes.
Assuming that the maximum buffer size of intermediate node $ \mathbf{R_1} $ in
Fig.~\ref{fig:concept} is $ l = [\mathbf{B_1}] = n + r $, the probability of having all
non-zeros in a recoding coefficient vector $ \gamma_k $ in
Eqn.~(\ref{eq:inner_recoding}) is $ l/2 $. It means that the $ l/2 $ received
packets in the buffer $ \mathbf{B_1} $ are recombined together on average. This enhances the
robustness of coded packets and increases the number of linear independent coded
packets at the decoder. 

On the other side, with the unlimited buffer recoding, the intermediate nodes can recreate the original packets $ P $ and generate the $ r $ expansion packets that are missing in the inner code, if the node collects $ n $ linearly independent inner-coded packets and then maps back to the high field $ GF(2^h) $ in order to decode that data with outer or combined decoding \cite{Lucani2018}. This enhances the quality of the recoded packets and speeds up the transmission process. However, this optional mechanism can only be useful if the intermediate nodes are allowed to trade off throughput performance with complexity.

On the other hand, by letting all the received packets in the buffer to be
recombined together, the coding density at each hop in the network increases.
There is an issue that if the recoding is employed at multiple hops, then the coded
packets become denser and denser, and the receiver no longer performs a sparse
decoding. Thus, all the advantages of sparse encoding at the sender, which are
composed from different schemes i.e., \mbox{S-RLNC}~\cite{garrido2017recoding}, DSEP~\cite{nguyen2020dsep}, seem to be diluted by the recoding process, especially with unlimited buffers.

To better understand the recoding process, let's consider an example in
Fig.~\ref{fig:recoding} with $ n= 8 $ and $r =  2 $. Let's assume that after
the outer and inner encoding processes, which are employing the static sparsity
$ w = 3 $, the intermediate node receives and stores the first two received
inner-coded packets $ i_1 $ and $ i_2 $ with the coding coefficients  $ \{1, 0, 1,
1, 0, 0, 0, 0, 0, 0\}$ and $ \{0, 1, 0, 0, 0, 1, 0, 0, 1, 0\}$ in $ GF(2) $,
respectively. These inner-coded packets are recoded at the intermediate node
with a new coding coefficient vector such as $ \gamma_2  = \{1, 1\}$  in order
to create a new recoded packet $ i'_2 $ as 
\begin{equation}
i'_2 = 1i_1 + 1i_2,
\end{equation}
which can be rewritten as
\begin{equation}
i'_2 = 1p_1 + 1p_3 + 1p_4 + 1p_2+ 1p_6 + 1o_1.
\label{eq:recoding}
\end{equation}
Eqn.~(\ref{eq:recoding}) shows the density significantly increasing to 6 when the
intermediate node recodes the received inner-coded packets. In particular, for the unlimited buffer recoding, the
intermediate node is storing all received inner-coded packets for a given
generation tending to cumulatively increase the density level. With more and more
inner-coded packets are received and stored for a generation, more and more
packets are recombined with each other in the intermediate nodes. This leads to
a reduction of the decoding throughput at computing-limited receivers.

\subsection{A limited buffer recoding}
\label{subsec:lim_buf}
\begin{figure}[!t]
	\centering
	\includegraphics[width=3.35in]{figures/ldp_with_DSEP_relay4_density_0.3.eps}
	\caption{Overhead of different schemes using DSEP recoding (red lines) and the conventional recoding with different queue sizes for $ n = 32 $, $ r = 2 $, $ d = 0.3 $ and $ m = 4 $.}
	\label{fig:overhead}
	\vspace{-1.5em}
\end{figure}
% Please add the following required packages to your document preamble:
% \usepackage{multirow}

To deal with the issues of the unlimited buffer recoding and preserve the
advantages of the sparse network coding schemes particularly DSEP scheme, DSEP
requires a proper recoding mechanism that allows intermediate nodes storing
a small amount of coded packets in the buffer to efficiently recode the packets.
The recoding mechanism must help not only to increase the performance of
recoding and decoding, but also to reduce the memory at the intermediate nodes.
In order to evaluate how big the buffer size should be for a given generation,
we examine the different buffer sizes (or maximum number of packets stored
at an intermediate node) with different schemes, as shown in
Fig.~\ref{fig:overhead}. This figure shows that the decoding overhead (defined
as the ratio between the number of linear dependent packets and the original
packets $ n $) is of 100\% with different buffer sizes $ l $ for the conventional recoding and DSEP recoding in the case of four intermediate nodes. In the conventional recoding, the overhead is
reduced and maintained at the same level as we increase the buffer size to up
to 10 symbols. It means that we can not achieve a lower decoding overhead with
buffer sizes larger than 10 symbols. Additionally,
Table~\ref{table:buffer_gen} also shows the minimal decoding overhead that can
be achieved corresponding to the limited buffer sizes $ l $. These decoding
overhead are not reduced any more by increasing the buffer sizes, i.e., $ l = n
$. Therefore, we can conclude that the unlimited buffer is not a good choice for
either computing complexity or memory at the intermediate nodes.

We propose the novel recoding mechanism for DSEP scheme that allows the
intermediate nodes to not only control the storage of received inner-coded
packets in a limited buffer size, but also to maintain the low sparsity level of
the encoder during the recoding process. This recoding mechanism is sketched in
Fig.~\ref{fig:recoding}. A sender encodes and sends the inner-coded packets
through the lossy channel. The intermediate node employing DSEP recoding with limited buffer e.g., $ l=3
$, can store only a few recently received inner-coded packets in the order such
as $ i_1 $, $ i_2 $ and $ i_4 $. If the buffer is full and there is an available
incoming inner-coded packet e.g., $ i_5 $, the earliest coded packet in the
buffer e.g., $ i_1 $, will be removed from the buffer. Subsequently, the
intermediate node combines a small number of the recently received inner-coded
packets. The combinations depend on the buffer size $ l $ and the recoding
coefficient vectors, a new inner-recoded packet is formed as follows
\begin{equation}
i'_k = \sum\limits_{j=1}^{l} \gamma_{k,j} i_j,~ k=1, 2, ..., n,...,
\label{eq:new_inner_recoding}
\end{equation}
where $ \gamma_{k,j} $ are the inner recoding coefficient vectors. While $
\gamma_{k,j} $, $j=1,2,...,l-1 $, are randomly selected by an intermediate node
from $ GF(2) $, $ \gamma_{k,l} $ is set to one to ensure the latest
incoming inner-coded packets is always combined in a new recoded packet, and to
avoid all zeros coefficient vector generated. Furthermore, our recoding
mechanism also allows a first incoming coded packet e.g., $ i_1 $ in
Fig.~\ref{fig:recoding}, is forwarded to the next hop without recoding and a
second coded packet~e.g., $ i_2 $, is combined with the first one with
probability~$ 1/2 $.
\begin{table}[!t]
	%	\vspace{1.em}
	\caption{Minimal decoding overhead of the conventional $ GF(2) $ recoding with a proper buffer size as a function of generation sizes $ n $ for two intermediate nodes. Fixed parameteres: expansion packets $ r = 2 $, density for \mbox{S-RLNC} $ d = 0.3 $. }
	
	\label{table:buffer_gen}
	\scalebox{0.96}{
		\begin{tabular}{|l|l|c|c|c|c|c|c|}
			\hline
			\multicolumn{1}{|c|}{\textbf{Schemes}}                                        & \textbf{\begin{tabular}[c]{@{}l@{}}Buffer Size/\\ Overhead\end{tabular}} & \textbf{8} & \textbf{16} & \textbf{32} & \textbf{64} & \textbf{128} & \textbf{256} \\ \hline
			\multirow{2}{*}{\begin{tabular}[c]{@{}l@{}}Full RLNC GF(2)\end{tabular}}     & \begin{tabular}[c]{@{}l@{}}Buffer Size \end{tabular}                    & 6          & 8           & 10          & 12          & 14           & 15           \\ \cline{2-8} 
			& Overhead (\%)                                                                & 61         & 30          & 15          & 7           & 4            & 2            \\ \hline
			\multirow{2}{*}{\begin{tabular}[c]{@{}l@{}}S-RLNC GF(2)\end{tabular}}   & \begin{tabular}[c]{@{}l@{}}Buffer Size \end{tabular}                    & 7          & 9           & 11          & 13          & 14           & 15           \\ \cline{2-8} 
			& Overhead (\%)                                                                 & 61         & 30          & 15          & 7           & 4            & 2            \\ \hline
			\multirow{2}{*}{\begin{tabular}[c]{@{}l@{}}Fulcrum Inner\end{tabular}}       & \begin{tabular}[c]{@{}l@{}}Buffer Size \end{tabular}                    & 6          & 9           & 11          & 13          & 14           & 15           \\ \cline{2-8} 
			& Overhead (\%)                                                                & 67         & 38          & 20          & 10          & 5            & 2            \\ \hline
			\multirow{2}{*}{\begin{tabular}[c]{@{}l@{}}Fulcrum\\ Comb./outer\end{tabular}} & \begin{tabular}[c]{@{}l@{}}Buffer Size \end{tabular}                    & 6          & 8           & 10          & 12          & 13           & 14           \\ \cline{2-8} 
			& Overhead (\%)                                                                 & 41         & 22          & 11          & 5           & 3            & 1            \\ \hline
		\end{tabular}
	}
	%	\vspace{-0.3cm}
	\vspace{-1.5em}
\end{table}
\begin{figure*}[t]
	\centering
	\subfigure[Recoding Goodput]{
		\includegraphics[width=3.35in]{figures/re_goodput_withDSEP_rc_3_red_5_den_0.3_e1_0.05_e2_0.3.eps}
		\label{sfig:recoding_goodput}
	}
	%	\vspace{-0.9em}
	\hspace{.5em}
	\subfigure[Decoding Goodput]{
		\includegraphics[width=3.35in]{figures/de_goodput_withDSEP_rc_3_red_5_den_0.3_e1_0.05_e2_0.3.eps}
		\label{sfig:decoding_goodput}
	}
	%		\vspace{-0.9em}
	\caption{Recoding and decoding goodput of different schemes using DSEP recoding (red lines) and the conventional recoding with the limited queue size $ l = 3 $ and $ l = 10 $ coded packets, respectively.}
	\label{fig:goodput}
	\vspace{-1.5em}
\end{figure*}
To illustrate this recoding mechanism, let's turn back to the example in
Section~\ref{subsec:unlim_buf}. Assuming that the encoding process employs DSEP
scheme with a dynamic expansion packets and dynamic sparsity level. The sparsity
of two first inner-coded packets $ i_1 $ and $ i_2 $ is $ w(1) = 1$ and $ w(2) =
2 $, respectively. The coding coefficient vectors corresponding to packets $ i_1
$ and $ i_2 $ are $\{1, 0, 0, 0, 0, 0, 0, 0, 0, 0\}$ and $\{0, 1, 0, 1, 0, 0, 0, 0,
0, 0\}$. The first incoming coded packets $ i_1 $ will be stored and forwarded to
the next hop, the second one $ i_2 $ is also stored into the buffer and is
recoded with the first one. Assuming that the recoding coefficient vector is
also $ \gamma_2 = \{1, 1\}$. The new recoded packet $ i'_1 $ in
Eqn.~(\ref{eq:recoding}) is rewritten as follows
\begin{equation}
i'_2 = 1p_1 + 1p_2+ 1p_4
\label{eq:new_ex_recoding}
\end{equation}
It is clear that the density is also cumulative and slightly increased to 3 in
Eqn.~(\ref{eq:new_ex_recoding}) which is much lower than in
Eqn.~(\ref{eq:recoding}). Importantly, the potential problem of increasing coding
density is mitigated due to the limited buffer. We will show the advantages of
both decoding overhead and coding throughput in Section~\ref{sec:evaluation}.